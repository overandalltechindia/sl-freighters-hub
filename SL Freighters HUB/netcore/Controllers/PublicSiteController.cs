﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using netcore.Models;

namespace netcore.Controllers
{
    //[Authorize(Roles = "PublicSite")]
    public class PublicSiteController : Controller
    {
        private readonly ILogger _logger;

        public PublicSiteController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        //[Authorize]
        public IActionResult Index()
        {
            _logger.LogInformation(LoggingEvents.ListItems, "Listing Public Home Page");

            return View();
        }

        public IActionResult Contact()
        {
            _logger.LogInformation(LoggingEvents.ListItems, "Listing Public Contact Page");

            return View();
        }


    }
}

namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class PublicSite
        {
            public const string Controller = "PublicSite";
            public const string Action = "Index";
            public const string Role = "PublicSite";
            public const string Url = "/PublicSite/Index";
            public const string Name = "PublicSite";
        }
    }
}
//namespace netcore.Models
//{
//    public partial class ApplicationUser
//    {
//        [Display(Name = "PublicSite")]
//        public bool PublicSiteRole { get; set; } = false;
//    }
//}
