﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    //[Authorize(Roles = "BookingCustomerDocsLine")]
    public class BookingCustomerDocsLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingCustomerDocsLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BookingCustomerDocsLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BookingCustomerDocsLine.Include(x => x.booking).Include(x => x.customer);
            return View(await applicationDbContext.ToListAsync());
        }


        // GET: BookingCustomerDocsLine/Create
        public IActionResult Create(string masterid, string id)
    {
        var check = _context.BookingCustomerDocsLine.SingleOrDefault(m => m.bookingCustomerDocsLineId == id);
        var selected = _context.Booking.Include(x => x.serviceProvider).SingleOrDefault(m => m.quotationId == masterid);
        ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
        ViewData["customerId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "businessContactName");
        if (check == null)
        {
            BookingCustomerDocsLine objline = new BookingCustomerDocsLine();
            objline.booking = selected;
            objline.bookingId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }

    }


    // POST: BookingCustomerDocsLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(BookingCustomerDocsLine bookingCustomerDocsLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(bookingCustomerDocsLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        return View(bookingCustomerDocsLine);
    }


    private bool PointOfContactsLineExists(string id)
    {
        return _context.PointOfContactsLine.Any(e => e.pointOfContactLineId == id);
    }

  }
}





//namespace netcore.MVC
//{
//  public static partial class Pages
//  {
//      public static class PointOfContactsLinePointOfContactsLine
//      {
//          public const string Controller = "BookingServiceProviderDocsLine";
//          public const string Action = "Index";
//          public const string Role = "BookingServiceProviderDocsLine";
//          public const string Url = "/BookingServiceProviderDocsLine/Index";
//          public const string Name = "BookingServiceProviderDocsLine";
//      }
//  }
//}
//namespace netcore.Models
//{
//  public partial class ApplicationUser
//  {
//      [Display(Name = "BookingServiceProviderDocsLine")]
//      public bool BookingServiceProviderDocsLineRole { get; set; } = false;
//  }
//}



