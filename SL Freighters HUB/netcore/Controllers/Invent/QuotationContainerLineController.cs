﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "QuotationContainerLine")]
    public class QuotationContainerLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuotationContainerLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: QuotationContainerLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.QuotationContainerLine.Include(r => r.quotation);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: QuotationContainerLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotationContainerLine = await _context.QuotationContainerLine
                    .Include(r => r.quotation)
                        .SingleOrDefaultAsync(m => m.quotationContainerLineId == id);
            if (quotationContainerLine == null)
            {
                return NotFound();
            }

            return View(quotationContainerLine);
        }


        // GET: QuotationContainerLine/Create
        public IActionResult Create(string masterid, string id)
        {
            var check = _context.QuotationContainerLine.SingleOrDefault(m => m.quotationContainerLineId == id);
            var selected = _context.Quotation.SingleOrDefault(m => m.quotationId == masterid);
            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber");

            if (check == null)
            {
                QuotationContainerLine objline = new QuotationContainerLine();
                objline.quotation = selected;
                objline.quotationId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: QuotationContainerLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("quotationContainerLineId,quotationId,serviceType,containerType,commodityType,volume,weight,size,containerLength,containerWidth,containerHeight,count,createdAt")] QuotationContainerLine quotationContainerLine)
        {
            
            if (ModelState.IsValid)
            {
                _context.Add(quotationContainerLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber", quotationContainerLine.quotationId);

            return View(quotationContainerLine);
        }

        // GET: QuotationContainerLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotationContainerLine = await _context.QuotationContainerLine.SingleOrDefaultAsync(m => m.quotationContainerLineId == id);
            if (quotationContainerLine == null)
            {
                return NotFound();
            }

            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber", quotationContainerLine.quotationId);
            return View(quotationContainerLine);
        }

        // POST: QuotationContainerLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("quotationContainerLineId,quotationId,serviceType,containerType,commodityType,volume,weight,size,containerLength,containerWidth,containerHeight,count,createdAt")] QuotationContainerLine quotationContainerLine)
        {
            if (id != quotationContainerLine.quotationContainerLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quotationContainerLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuotationContainerLineExists(quotationContainerLine.quotationContainerLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber", quotationContainerLine.quotationId);

            return View(quotationContainerLine);
        }

        // GET: QuotationContainerLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotationContainerLine = await _context.QuotationContainerLine
                    .Include(r => r.quotation)
                    .SingleOrDefaultAsync(m => m.quotationContainerLineId == id);
            if (quotationContainerLine == null)
            {
                return NotFound();
            }

            return View(quotationContainerLine);
        }




        // POST: QuotationContainerLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var quotationContainerLine = await _context.QuotationContainerLine.SingleOrDefaultAsync(m => m.quotationContainerLineId == id);
            _context.QuotationContainerLine.Remove(quotationContainerLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuotationContainerLineExists(string id)
        {
            return _context.QuotationContainerLine.Any(e => e.quotationContainerLineId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class QuotationContainerLine
        {
            public const string Controller = "QuotationContainerLine";
            public const string Action = "Index";
            public const string Role = "QuotationContainerLine";
            public const string Url = "/QuotationContainerLine/Index";
            public const string Name = "QuotationContainerLine";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "QuotationContainerLine")]
        public bool QuotationContainerLineRole { get; set; } = false;
    }
}



