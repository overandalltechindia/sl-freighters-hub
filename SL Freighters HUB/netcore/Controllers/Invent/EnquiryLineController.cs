﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "EnquiryLine")]
    public class EnquiryLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EnquiryLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EnquiryLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.EnquiryLine.Include(p => p.enquiry);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: EnquiryLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquiryLine = await _context.EnquiryLine
                    .Include(p => p.enquiry)
                        .SingleOrDefaultAsync(m => m.enquiryLineId == id);
            if (enquiryLine == null)
            {
                return NotFound();
            }

            return View(enquiryLine);
        }


        // GET: EnquiryLine/GetINRPrice/5
        //[HttpGet]
        //public decimal? GetINRPrice(string productId, string enquiryId)
        //{
        //    if (productId == null)
        //    {
        //        return null;
        //    }
        //    if (enquiryId == null)
        //    {
        //        return null;
        //    }

        //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
        //    var usdRate = _context.Enquiry.SingleOrDefault(x => x.enquiryId == enquiryId).usdRate;
        //    var INRTotal = productPriceinUSD * usdRate;

        //    if (INRTotal == null)
        //    {
        //        return null;
        //    }
        //    return INRTotal;
        //}



        // POST: EnquiryLine/PostEnquiryLine
        //[HttpPost]

        public async Task<IActionResult> PostEnquiryLine(EnquiryLine enquiryLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!SameEnquiryLineDataExists(enquiryLine))
            {
                if (enquiryLine.enquiryLineId == string.Empty)
                {
                    enquiryLine.enquiryLineId = Guid.NewGuid().ToString();
                    _context.EnquiryLine.Add(enquiryLine);
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, message = "Added New Enquiry Information" });
                }
                else
                {
                    _context.Update(enquiryLine);
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, message = "Edited Enquiry Information" });
                }
            }
            else
            {
                return Json(new { success = false, message = "Same Enquiry Information Exists" });
            }

        }



        // GET: EnquiryLine/Create
        public IActionResult Create(string masterid, string id)
        {
            var check = _context.EnquiryLine.SingleOrDefault(m => m.enquiryLineId == id);
            var selected = _context.Enquiry.SingleOrDefault(m => m.enquiryId == masterid);
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryId");
            if (check == null)
            {
                EnquiryLine objline = new EnquiryLine();
                objline.enquiry = selected;
                objline.enquiryId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: EnquiryLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("enquiryLineId,enquiryId,serviceType,containerType,commodityType,volume,weight,size,containerLength,containerWidth,containerHeight,count,createdAt")] EnquiryLine enquiryLine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enquiryLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryId", enquiryLine.enquiryId);


            return View(enquiryLine);
        }

        // GET: EnquiryLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquiryLine = await _context.EnquiryLine.SingleOrDefaultAsync(m => m.enquiryLineId == id);
            if (enquiryLine == null)
            {
                return NotFound();
            }
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryId", enquiryLine.enquiryId);
            return View(enquiryLine);
        }

        // POST: EnquiryLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("enquiryLineId,enquiryId,serviceType,containerType,commodityType,volume,weight,size,containerLength,containerWidth,containerHeight,count,createdAt")] EnquiryLine enquiryLine)

        {
            if (id != enquiryLine.enquiryLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(enquiryLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EnquiryLineExists(enquiryLine.enquiryLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryId", enquiryLine.enquiryId);
            return View(enquiryLine);
        }

        // GET: EnquiryLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquiryLine = await _context.EnquiryLine
                    .Include(p => p.enquiry)
                    .SingleOrDefaultAsync(m => m.enquiryLineId == id);
            if (enquiryLine == null)
            {
                return NotFound();
            }

            return View(enquiryLine);
        }




        // POST: EnquiryLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var enquiryLine = await _context.EnquiryLine.SingleOrDefaultAsync(m => m.enquiryLineId == id);
            _context.EnquiryLine.Remove(enquiryLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EnquiryLineExists(string id)
        {
            return _context.EnquiryLine.Any(e => e.enquiryLineId == id);
        }


        private bool SameEnquiryLineDataExists(EnquiryLine enquiryLine)
        {
            return _context.EnquiryLine.Any(e => e.serviceType == enquiryLine.serviceType &&
                                                    e.loadType == enquiryLine.loadType &&
                                                    e.commodityType == enquiryLine.commodityType && 
                                                    e.count == enquiryLine.count && 
                                                    e.weight == enquiryLine.weight &&
                                                    e.weightUOM == enquiryLine.weightUOM &&
                                                    e.isRefrigerated == enquiryLine.isRefrigerated &&
                                                    e.temperature == enquiryLine.temperature);
        }

    }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class EnquiryLine
      {
          public const string Controller = "EnquiryLine";
          public const string Action = "Index";
          public const string Role = "EnquiryLine";
          public const string Url = "/EnquiryLine/Index";
          public const string Name = "EnquiryLine";
      }
  }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "EnquiryLine")]
        public bool EnquiryLineRole { get; set; } = false;
    }
}



