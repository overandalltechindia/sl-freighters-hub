﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Models.ScreenViewModels;
using netcore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using netcore.Models;

namespace netcore.Controllers.Invent
{


    //[Authorize(Roles = "Analytics")]
    public class AnalyticsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _netcoreService;
        private readonly UserManager<ApplicationUser> _userManager;


        public AnalyticsController(ApplicationDbContext context, INetcoreService netcoreService, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _netcoreService = netcoreService;
            _userManager = userManager;

        }


        // GET: Analytics

        public async Task<IActionResult> Index()
        {
            var allVerifiedCustomers = _context.Customer.Where(x => x.isVerified == true).ToList();
            List<AnalyticsViewModel> viewModel = new List<AnalyticsViewModel>();
            List<AnalyticsViewModel> data = new List<AnalyticsViewModel>();

      
            var enquiryData = _context.Enquiry.Include(x => x.enquiryLine).Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).OrderByDescending(x => x.createdAt).ToList();

            var quotationData = _context.Quotation.OrderByDescending(x => x.createdAt).Include(r => r.enquiry).ThenInclude(x => x.enquiryLine).Include(r =>r.enquiry.originOfShipment).Include(r => r.enquiry.destinationOfShipment).Include(r => r.originOfShipment).Include(r => r.destinationOfShipment).Include(x=> x.quotationChargesLine).ToList();

            var bookingData = _context.Booking.OrderByDescending(x => x.createdAt).Include(x => x.quotation).Include(r => r.quotation).Include(r => r.quotation.enquiry).ThenInclude(x =>x.enquiryLine).Include(r => r.originOfShipment).Include(r => r.destinationOfShipment).ToList();

            var model = new AnalyticsViewModel();

            var serviceProviderList = _context.ServiceProvider.Include(x => x.servicesLine).ToList();



            foreach (var item2 in enquiryData)
            {
                var serviceProvidersReceivedEnquiryList = serviceProviderList.Where(x => x.servicesLine.Select(z => z.serviceType).FirstOrDefault() == item2.enquiryLine.Select(y => y.serviceType).FirstOrDefault()).ToList();


                if (serviceProvidersReceivedEnquiryList.Count > 0)
                {
                    model.serviceProvidersReceivedEnquiryList = serviceProvidersReceivedEnquiryList;
                }

            }


            foreach (var item3 in quotationData)
            {
                var serviceProvidersSubmittedQuote = serviceProviderList.FirstOrDefault(x => x.serviceProviderId == item3.serviceProviderId && item3.enquiryId == item3.enquiryId);

                if (serviceProvidersSubmittedQuote != null)
                {
                    model.serviceProvidersSubmittedQuoteList.Add(serviceProvidersSubmittedQuote);
                }
            }


            if (enquiryData != null)
            {
                model.enquiryList = enquiryData;
            }
            if (quotationData != null)
            {
                model.quotationList = quotationData;
            }
            if (bookingData != null)
            {
                model.bookingList = bookingData;
            }
            
            
            data.Add(model);
            viewModel = data;
            

            return View(viewModel);
        }


        public IActionResult GetTrackingLink(string shipmentLineName)
        {
            var trackingLink = _context.ShipmentTrackingLinks.FirstOrDefault(x => x.shipmentName.Equals(shipmentLineName)).shipmentTrackingLink;
            
            return Json(trackingLink);
        }

        // GET: Analytics/Details/5
        //public IActionResult Details(string id, bool isFinished = false)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
            
        //    ViewData["StatusMessage"] = TempData["StatusMessage"];
        //    List<Analytics> analyticsList = _context.Analytics.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.analyticsExpiryDate >= DateTime.Now).ToList();

        //    ViewData["analyticsId"] = new SelectList(analyticsList, "analyticsId", "analyticsNumber", id);

        //    var analyticsByQuotation = _context.Analytics.Include(x => x.shipmentTrackingLinks).Include(x => x.quotation.enquiry).ThenInclude(x => x.enquiryLine).Include(x => x.customer).Include(x => x.quotation).Include(x => x.quotation.serviceProvider).Include(x => x.analyticsServiceProviderDocsLine).FirstOrDefault(x => x.analyticsId == id);

        //    var appUser = _userManager.GetUserAsync(User).Result;

        //    if (appUser.isServiceProvider)
        //    {
        //        if (analyticsByQuotation.analyticsStatus == AnalyticsStatus.New)
        //        {
        //            analyticsByQuotation.analyticsStatus = AnalyticsStatus.Ongoing;
        //            _context.SaveChanges();
        //        }
        //    }
           

        //    var analytics = new Analytics();
        //    analytics.analyticsStatus = analyticsByQuotation.analyticsStatus;
        //    if (analyticsByQuotation != null)
        //    {
        //        //analytics.quotation.enquiry.enquiryLine = analyticsByQuotation.quotation.enquiry.enquiryLine;
        //        analytics.quotation = analyticsByQuotation.quotation;
        //        analytics.analyticsId = analyticsByQuotation.analyticsId;
        //        analytics.quotationId = analyticsByQuotation.quotationId;
        //        analytics.customer = analyticsByQuotation.customer;
        //        analytics.customerId = analyticsByQuotation.customerId;
        //        analytics.serviceProvider = analyticsByQuotation.quotation.serviceProvider;
        //        analytics.serviceProvider.companyName = analyticsByQuotation.quotation.serviceProvider.companyName;
        //        analytics.analyticsNumber = analyticsByQuotation.analyticsNumber;
        //        analytics.etd = analyticsByQuotation.etd;
        //        analytics.eta = analyticsByQuotation.eta;
        //        analytics.transitDays = analyticsByQuotation.transitDays;
        //        analytics.expeditionType = analyticsByQuotation.expeditionType;
        //        analytics.shipmentTrackingLinks = analyticsByQuotation.shipmentTrackingLinks;
        //        analytics.trackingLink = analyticsByQuotation.trackingLink;
        //        analytics.trackingReferenceNo = analyticsByQuotation.trackingReferenceNo;
        //        analytics.terms = analyticsByQuotation.terms;
        //        analytics.additionalInformation = analyticsByQuotation.additionalInformation;
        //        analytics.analyticsChargesLine = analyticsByQuotation.analyticsChargesLine;
        //        analytics.analyticsServiceProviderDocsLine = analyticsByQuotation.analyticsServiceProviderDocsLine;

        //    }
     
        //    //analytics.originOfShipmentLocationName = analyticsByQuotation.originOfShipment.locationName;
        //    //analytics.destinationOfShipmentLocationName = analyticsByQuotation.originOfShipment.locationName;

            

        //    return View(analytics);
        //}


        //public IActionResult AnalyticsView(string analyticsId)
        //{
        //    var analyticsView = new AnalyticsViewModel();
        //    var analyticsChargesLineView = new List<AnalyticsChargesLineViewModel>();

        //    if (analyticsId != null)
        //    {
        //        var analyticsModel = _context.Analytics.FirstOrDefault(x => x.analyticsId == analyticsId);

        //        var analyticsChargesLine = _context.AnalyticsChargesLine.Where(x => x.analyticsId == analyticsId).ToList();
        //    }

        //    return PartialView("AnalyticsView", analyticsView);
        //}

    }
}




//namespace netcore.MVC
//{
//    public static partial class Pages
//    {
//        public static class Analytics
//        {
//            public const string Controller = "Analytics";
//            public const string Action = "Index";
//            public const string Role = "Analytics";
//            public const string Url = "/Analytics/Index";
//            public const string Name = "Analytics";
//        }
//    }
//}
//namespace netcore.Models
//{
//    public partial class ApplicationUser
//    {
//        [Display(Name = "Analytics")]
//        public bool AnalyticsRole { get; set; } = false;
//    }
//}



