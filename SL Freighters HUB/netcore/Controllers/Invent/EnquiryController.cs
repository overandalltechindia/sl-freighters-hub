﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Models.ScreenViewModels;
using netcore.Services;
using netcore.Models;
using Microsoft.AspNetCore.Identity;

namespace netcore.Controllers.Invent
{

    [DisplayName("Enquiry")]
    [Authorize(Roles = "Enquiry")]
    public class EnquiryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _netcoreService;
        private readonly UserManager<ApplicationUser> _userManager;


        public EnquiryController(ApplicationDbContext context, INetcoreService netcoreService, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _netcoreService = netcoreService;
            _userManager = userManager;
        }

        public async Task<IActionResult> ShowEnquiry(string id)
        {
            Enquiry obj = await _context.Enquiry
                .Include(x => x.originOfShipment)
                .Include(x => x.destinationOfShipment)
                .Include(x => x.enquiryLine).ThenInclude(x => x.enquiry)
                .SingleOrDefaultAsync(x => x.enquiryId.Equals(id));

            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}


        public async Task<IActionResult> PrintEnquiry(string id)
        {
            Enquiry obj = await _context.Enquiry
                 .Include(x => x.originOfShipment)
                .Include(x => x.destinationOfShipment)
                .Include(x => x.enquiryLine).ThenInclude(x => x.enquiry)
                .SingleOrDefaultAsync(x => x.enquiryId.Equals(id));
            return View(obj);
        }

        // GET: Enquiry
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Enquiry.Include(x => x.enquiryLine).Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).OrderByDescending(x => x.createdAt);
            return View(await applicationDbContext.ToListAsync());
        }


        // GET: Enquiry/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquiry = await _context.Enquiry
                    .Include(x => x.enquiryLine)
                    .Include(p => p.originOfShipment)
                    .Include(x => x.destinationOfShipment)
                        .SingleOrDefaultAsync(m => m.enquiryId == id);
            if (enquiry == null)
            {
                return NotFound();
            }

            //enquiry.totalOrderAmount = enquiry.enquiryLine.Sum(x => x.totalAmount);
            //enquiry.totalDiscountAmount = enquiry.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(enquiry);
            await _context.SaveChangesAsync();

            return View(enquiry);
        }


        public IActionResult GetLocationOnLocationType(string locationType)
        {
            var locationList = new List<Location>();

            if (locationType != null)
            {
                locationList = _context.Location.Where(x => x.locationType.Equals(locationType)).ToList();
            }
            //warehouseList.Insert(0, new Warehouse { warehouseId = "0", warehouseName = "Select" });

            return Json(new SelectList(locationList.Select(u => new {
                locationId = u.locationId,
                locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
            }), "locationId", "locationName"));
        }


        // GET: Enquiry/Create
        public IActionResult Create()
        {
            EnquiryViewModel en = new EnquiryViewModel();
            //ViewData["fclOriginOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Sea Port"), "locationId", "locationName");
            ViewData["locationType"] = Enum.GetValues(typeof(LocationType))
                        .Cast<LocationType>()
                        .Where(e => e != LocationType.Airport && e != LocationType.Business && e != LocationType.Residential)
                        .Select(e => new SelectListItem
                        {
                            Value = ((int)e).ToString(),
                            Text = e.GetDisplayName()
                        });


            ViewData["airLocationType"] = Enum.GetValues(typeof(LocationType))
                        .Cast<LocationType>()
                        .Where(e => e != LocationType.Port && e != LocationType.Business && e != LocationType.Residential)
                        .Select(e => new SelectListItem
                        {
                            Value = ((int)e).ToString(),
                            Text = e.GetDisplayName()
                        });

            ViewData["ftlOriginOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == LocationType.Factory_Warehouse.GetDisplayName()).ToList()
                                         .Select(u => new
                                         {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");
            ViewData["ftlDestinationOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == LocationType.Factory_Warehouse.GetDisplayName()).ToList()
                                         .Select(u => new
                                         {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");


            ViewData["ltlOriginOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == LocationType.Factory_Warehouse.GetDisplayName()).ToList()
                                         .Select(u => new
                                         {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");
            ViewData["ltlDestinationOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == LocationType.Factory_Warehouse.GetDisplayName()).ToList()
                                         .Select(u => new
                                         {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");

            return View(en);
        }


        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> SendEnquiry(EnquiryViewModel enquiryViewModel, List<EnquiryLine> enquiryViewModelLine)
        {
            var enquiry = new Enquiry();
            enquiry.enquiryNumber = enquiryViewModel.enquiryNumber;
            TimeSpan time = DateTime.Now.TimeOfDay;
            enquiry.enquiryDate = enquiryViewModel.enquiryDate + time;
            enquiry.enquiryExpiryDate = enquiryViewModel.enquiryExpiryDate + time;

            enquiry.hasCustomClearance = enquiryViewModel.hasCustomClearance;
            enquiry.comments = enquiryViewModel.comments;
            enquiry.userName = User.Identity.Name;
            enquiry.enquiryStatus = EnquiryStatus.Pending;      //DEFAULT STATUS
            enquiry.createdAt = DateTime.Now;    

            var fclServiceType = enquiryViewModelLine.Any(x => x.serviceType == ServicesType.FCL);
            if (fclServiceType)
            {
                enquiry.originOfShipmentId = enquiryViewModel.fclOriginOfShipmentId;
                enquiry.destinationOfShipmentId = enquiryViewModel.fclDestinationOfShipmentId;

                enquiry.originLocationType = enquiryViewModel.fclOriginLocationType;
                enquiry.destinationLocationType = enquiryViewModel.fclDestinationLocationType;
                enquiry.pickupAddressPincode = enquiryViewModel.fclPickupAddressPincode;
                enquiry.deliveryAddressPincode = enquiryViewModel.fclDeliveryAddressPincode;
                enquiry.modeOfTransport = ModeOfTransport.Ocean;
            }
            var lclServiceType = enquiryViewModelLine.Any(x => x.serviceType == ServicesType.LCL);
            if (lclServiceType)
            {
                enquiry.originOfShipmentId = enquiryViewModel.lclOriginOfShipmentId;
                enquiry.destinationOfShipmentId = enquiryViewModel.lclDestinationOfShipmentId;

                enquiry.originLocationType = enquiryViewModel.lclOriginLocationType;
                enquiry.destinationLocationType = enquiryViewModel.lclDestinationLocationType;
                enquiry.pickupAddressPincode = enquiryViewModel.lclDeliveryAddressPincode;
                enquiry.deliveryAddressPincode = enquiryViewModel.lclDeliveryAddressPincode;
                enquiry.modeOfTransport = ModeOfTransport.Ocean;
            }
            var airServiceType = enquiryViewModelLine.Any(x => x.serviceType == ServicesType.Air);
            if (airServiceType)
            {
                //AIR
                enquiry.originOfShipmentId = enquiryViewModel.airOriginOfShipmentId;
                enquiry.destinationOfShipmentId = enquiryViewModel.airDestinationOfShipmentId;

                enquiry.originLocationType = enquiryViewModel.airOriginLocationType;
                enquiry.destinationLocationType = enquiryViewModel.airDestinationLocationType;
                enquiry.pickupAddressPincode = enquiryViewModel.airDeliveryAddressPincode;
                enquiry.deliveryAddressPincode = enquiryViewModel.airDeliveryAddressPincode;
                enquiry.modeOfTransport = ModeOfTransport.Air;
            }
            var ftlServiceType = enquiryViewModelLine.Any(x => x.serviceType == ServicesType.FTL);
            if (ftlServiceType)
            {
                //FTL
                enquiry.originOfShipmentId = enquiryViewModel.ftlOriginOfShipmentId;
                enquiry.destinationOfShipmentId = enquiryViewModel.ftlDestinationOfShipmentId;
                enquiry.pickupAddressPincode = enquiryViewModel.ftlPickupAddressPincode;
                enquiry.deliveryAddressPincode = enquiryViewModel.ftlDeliveryAddressPincode;

                enquiry.modeOfTransport = ModeOfTransport.Road;
            }
            var ltlServiceType = enquiryViewModelLine.Any(x => x.serviceType == ServicesType.LTL);
            if (ltlServiceType)
            {
                //LTL
                enquiry.originOfShipmentId = enquiryViewModel.ltlOriginOfShipmentId;
                enquiry.destinationOfShipmentId = enquiryViewModel.ltlDestinationOfShipmentId;
                enquiry.pickupAddressPincode = enquiryViewModel.ltlPickupAddressPincode;
                enquiry.deliveryAddressPincode = enquiryViewModel.ltlDeliveryAddressPincode;

                enquiry.modeOfTransport = ModeOfTransport.Road;
            }
            var railServiceType = enquiryViewModelLine.Any(x => x.serviceType == ServicesType.RAIL);
            if (railServiceType)
            {
                //RAIL
                enquiry.pickupAddressPincode = enquiryViewModel.railPickupAddressPincode;
                enquiry.deliveryAddressPincode = enquiryViewModel.railDeliveryAddressPincode;

                enquiry.modeOfTransport = ModeOfTransport.Rail;
            }
            

            _context.Add(enquiry);

            var notification = new Notifications();
            notification.Name = NotificationNameModel.EnquiryNotification;
            notification.fromUserId = _userManager.GetUserId(User);
            notification.toUserId = null;
            notification.Link = NotificationLinkModel.EnquiriesLink;
            notification.Data = NotificationMessageModel.NewEnquiryMessage;
            await _netcoreService.SendUserNotification(notification);
            await _context.SaveChangesAsync();

            foreach (var item in enquiryViewModelLine)
            {
                var enquiryLine = new EnquiryLine();
                enquiryLine.enquiryLineId = Guid.NewGuid().ToString();
                enquiryLine.enquiryId = enquiry.enquiryId;
                enquiryLine.serviceType = item.serviceType;
                if (item.isClosedBodyTruck)
                {
                    enquiryLine.isClosedBodyTruck = item.isClosedBodyTruck;
                    enquiryLine.closedLoadType = item.closedLoadType;
                }
                else
                {
                    enquiryLine.loadType = item.loadType;
                }
                enquiryLine.containerType = item.containerType;
                enquiryLine.packageType = item.packageType;
                enquiryLine.commodityType = item.commodityType;
                enquiryLine.volume = item.volume;
                enquiryLine.weight = item.weight;
                enquiryLine.weightUOM = item.weightUOM;
                enquiryLine.isRefrigerated = item.isRefrigerated;
                enquiryLine.temperature = item.temperature;
                enquiryLine.containerLength = item.containerLength;
                enquiryLine.containerWidth = item.containerWidth;
                enquiryLine.containerHeight = item.containerHeight;
                enquiryLine.count = item.count;
                enquiryLine.createdAt = DateTime.Now + time;
                _context.EnquiryLine.Add(enquiryLine);
            }

            await _context.SaveChangesAsync();

            //return Redirect("~/Enquiry/Index");
            return Json(new { success = true });
        }



        // POST: Enquiry/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EnquiryViewModel enquiry)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enquiry);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = enquiry.enquiryId });
            }
            ViewData["fclOriginOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Sea Port").ToList()
                                        .Select(u => new {
                                            locationId = u.locationId,
                                            locationName = String.Concat(u.locationName + " (" + u.code + ", " + u.countryProvince + ")")
                                        }),
                                            "locationId", "locationName");
            ViewData["fclDestinationOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Sea Port").ToList()
                                         .Select(u => new {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.code + ", " + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");


            ViewData["lclOriginOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Sea Port").ToList()
                                         .Select(u => new {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.code + ", " + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");
            ViewData["lclDestinationOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Sea Port").ToList()
                                         .Select(u => new {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.code + ", " + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");


            ViewData["airOriginOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Air Port").ToList()
                                         .Select(u => new {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.code + ", " + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");
            ViewData["airDestinationOfShipmentId"] = new SelectList(_context.Location.Where(x => x.locationType == "Air Port").ToList()
                                         .Select(u => new {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.code + ", " + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName");
            return View(enquiry);
        }

        // GET: Enquiry/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquiry = await _context.Enquiry.Include(x => x.enquiryLine).SingleOrDefaultAsync(m => m.enquiryId == id);
            if (enquiry == null)
            {
                return NotFound();
            }
            ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiry.originOfShipmentId);
            ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiry.destinationOfShipmentId);
            _context.Update(enquiry);
            await _context.SaveChangesAsync();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", purchaseOrder.branchId);
            //ViewData["vendorId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "companyName", purchaseOrder.vendorId);
            //TempData["PurchaseOrderStatus"] = enquiry.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(enquiry);
        }

        // POST: Enquiry/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("enquiryId,enquiryNumber,enquiryDate,enquiryExpiryDate,originOfShipmentId,destinationOfShipmentId,comments,HasChild,createdAt")] Enquiry enquiry)
        {
            if (id != enquiry.enquiryId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(enquiry);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EnquiryExists(enquiry.enquiryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiry.originOfShipmentId);
            ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiry.destinationOfShipmentId);
            return View(enquiry);
        }

        // GET: Enquiry/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquriy = await _context.Enquiry
                    .Include(x => x.enquiryLine)
                    .Include(p => p.originOfShipment)
                    .Include(p => p.destinationOfShipment)
                    .SingleOrDefaultAsync(m => m.enquiryId == id);
            if (enquriy == null)
            {
                return NotFound();
            }

            //enquriy.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //enquriy.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(enquriy);
            await _context.SaveChangesAsync();

            return View(enquriy);
        }




        // POST: Enquiry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var enquriy = await _context.Enquiry
                .Include(x => x.enquiryLine)
                .SingleOrDefaultAsync(m => m.enquiryId == id);
            try
            {
                _context.EnquiryLine.RemoveRange(enquriy.enquiryLine);
                _context.Enquiry.Remove(enquriy);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(enquriy);
            }
            
        }

        private bool EnquiryExists(string id)
        {
            return _context.Enquiry.Any(e => e.enquiryId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class Enquiry
        {
            public const string Controller = "Enquiry";
            public const string Action = "Index";
            public const string Role = "Enquiry";
            public const string Url = "/Enquiry/Index";
            public const string Name = "Enquiry";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Enquiry")]
        public bool EnquiryRole { get; set; } = false;
    }
}



