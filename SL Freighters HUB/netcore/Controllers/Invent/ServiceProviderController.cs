﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Models;

namespace netcore.Controllers.Invent
{


    //[Authorize(Roles = "ServiceProvider")]
    public class ServiceProviderController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServiceProviderController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ServiceProvider
        public async Task<IActionResult> Index()
        {
            return View(await _context.ServiceProvider.Include(x => x.applicationUser).OrderByDescending(x => x.createdAt).ToListAsync());
        }

        // GET: ServiceProvider/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviceProvider = await _context.ServiceProvider.Include(x => x.applicationUser)
                        .SingleOrDefaultAsync(m => m.serviceProviderId == id);
            if (serviceProvider == null)
            {
                return NotFound();
            }

            return View(serviceProvider);
        }


        // GET: ServiceProvider/Create
        public IActionResult Create()
        {
            return View();
        }




        // POST: ServiceProvider/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("serviceProviderId,applicationUserId,companyName,description,description,officialAddress,businessContactName,primaryEmail,primaryContact,location,countryOfRegistration,gstNo,panNo,addressProofDocument,HasChild,createdAt")] ServiceProvider serviceProvider)
        {
            if (ModelState.IsValid)
            {
                _context.Add(serviceProvider);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = serviceProvider.serviceProviderId });
            }
            return View(serviceProvider);
        }

        // GET: ServiceProvider/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviceProvider = await _context.ServiceProvider.SingleOrDefaultAsync(m => m.serviceProviderId == id);
            if (serviceProvider == null)
            {
                return NotFound();
            }
            return View(serviceProvider);
        }

        // POST: Vendor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("serviceProviderId,applicationUserId,companyName,description,description,officialAddress,businessContactName,primaryEmail,primaryContact,location,countryOfRegistration,gstNo,panNo,addressProofDocument,HasChild,createdAt")] ServiceProvider serviceProvider)
        {
            if (id != serviceProvider.serviceProviderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(serviceProvider);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ServiceProviderExists(serviceProvider.serviceProviderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(serviceProvider);
        }





        public async Task<RedirectToActionResult> ApproveKYC(string id, ServiceProvider serviceProvider)
        {
            if (ModelState.IsValid)
            {
                var existing = await _context.ServiceProvider.SingleOrDefaultAsync(m => m.serviceProviderId == id);

                existing.isVerified = true;
                _context.Update(existing);
                await _context.SaveChangesAsync();

            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<RedirectToActionResult> DisapproveKYC(string id, ServiceProvider serviceProvider)
        {
            if (ModelState.IsValid)
            {
                var existing = await _context.ServiceProvider.SingleOrDefaultAsync(m => m.serviceProviderId == id);

                existing.isVerified = false;
                _context.Update(existing);
                await _context.SaveChangesAsync();

            }

            return RedirectToAction(nameof(Index));
        }



        // GET: ServiceProvider/Edit/5
        public async Task<IActionResult> KYCEdit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviceProvider = await _context.ServiceProvider.SingleOrDefaultAsync(m => m.serviceProviderId == id);
            if (serviceProvider == null)
            {
                return NotFound();
            }
            return View(serviceProvider);
        }

        // POST: Vendor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> KYCEdit(string id, [Bind("serviceProviderId,applicationUserId,countryOfRegistration,gstNo,panNo,addressProofDocument,HasChild,createdAt,isAcceptedTerms,isVerified")] ServiceProvider serviceProvider)
        //{
        //    if (id != serviceProvider.serviceProviderId)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            serviceProvider.isAcceptedTerms = serviceProvider.isAcceptedTerms;
        //            serviceProvider.isVerified = serviceProvider.isVerified;
        //            _context.Update(serviceProvider);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!ServiceProviderExists(serviceProvider.serviceProviderId))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(serviceProvider);
        //}



        // GET: ServiceProvider/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviceProvider = await _context.ServiceProvider
                    .SingleOrDefaultAsync(m => m.serviceProviderId == id);
            if (serviceProvider == null)
            {
                return NotFound();
            }

            return View(serviceProvider);
        }




        // POST: ServiceProvider/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var serviceProvider = await _context.ServiceProvider.Include(x => x.servicesLine).Include(x => x.pointOfContactsLine).SingleOrDefaultAsync(m => m.serviceProviderId == id);
            try
            {
                _context.ServicesLine.RemoveRange(serviceProvider.servicesLine);
                _context.PointOfContactsLine.RemoveRange(serviceProvider.pointOfContactsLine);
                _context.ServiceProvider.Remove(serviceProvider);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(serviceProvider);
            }
            
        }

        private bool ServiceProviderExists(string id)
        {
            return _context.ServiceProvider.Any(e => e.serviceProviderId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class ServiceProvider
        {
            public const string Controller = "ServiceProvider";
            public const string Action = "Index";
            public const string Role = "ServiceProvider";
            public const string Url = "/ServiceProvider/Index";
            public const string Name = "ServiceProvider";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "ServiceProvider")]
        public bool ServiceProviderRole { get; set; } = false;
    }
}



