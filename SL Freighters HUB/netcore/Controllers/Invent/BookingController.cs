﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Models.ScreenViewModels;
using netcore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using netcore.Models;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "Booking")]
    public class BookingController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _netcoreService;
        private readonly UserManager<ApplicationUser> _userManager;


        public BookingController(ApplicationDbContext context, INetcoreService netcoreService, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _netcoreService = netcoreService;
            _userManager = userManager;

        }

        //public IActionResult GetWarehouseByOrder(string purchaseOrderId)
        //{
        //    PurchaseOrder po = _context.PurchaseOrder
        //        .Include(x => x.branch)
        //        .Where(x => x.purchaseOrderId.Equals(purchaseOrderId)).FirstOrDefault();

        //    List<Warehouse> warehouseList = _context.Warehouse.Where(x => x.branchId.Equals(po.branch.branchId)).ToList();
        //    warehouseList.Insert(0, new Warehouse { warehouseId = "0", warehouseName = "Select" });

        //    return Json(new SelectList(warehouseList, "warehouseId", "warehouseName"));
        //}

        //public async Task<IActionResult> ShowGSRN(string id)
        //{
        //    Receiving obj = await _context.Receiving
        //        .Include(x => x.vendor)
        //        .Include(x => x.purchaseOrder)
        //            .ThenInclude(x => x.branch)
        //        .Include(x => x.receivingLine).ThenInclude(x => x.product)
        //        .SingleOrDefaultAsync(x => x.receivingId.Equals(id));
        //    return View(obj);
        //}

        //public async Task<IActionResult> PrintGSRN(string id)
        //{
        //    Receiving obj = await _context.Receiving
        //        .Include(x => x.vendor)
        //        .Include(x => x.purchaseOrder)
        //            .ThenInclude(x => x.branch)
        //        .Include(x => x.receivingLine).ThenInclude(x => x.product)
        //        .SingleOrDefaultAsync(x => x.receivingId.Equals(id));
        //    return View(obj);
        //}

        // GET: Booking

        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Booking.OrderByDescending(x => x.createdAt).Include(r => r.quotation).Include(r => r.quotation.enquiry).ThenInclude(x => x.enquiryLine).Include(r => r.originOfShipment).Include(r => r.destinationOfShipment);
            return View(await applicationDbContext.ToListAsync());
        }


        public IActionResult GetTrackingLink(string shipmentLineName)
        {
            var trackingLink = _context.ShipmentTrackingLinks.FirstOrDefault(x => x.shipmentName.Equals(shipmentLineName)).shipmentTrackingLink;
            
            return Json(trackingLink);
        }

        // GET: Booking/Details/5
        public IActionResult Details(string id, bool isFinished = false)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            ViewData["StatusMessage"] = TempData["StatusMessage"];
            List<Booking> bookingList = _context.Booking.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.bookingExpiryDate >= DateTime.Now).ToList();

            ViewData["bookingId"] = new SelectList(bookingList, "bookingId", "bookingNumber", id);

            var bookingByQuotation = _context.Booking.Include(x => x.shipmentTrackingLinks).Include(x => x.quotation.enquiry).ThenInclude(x => x.enquiryLine).Include(x => x.customer).Include(x => x.quotation).Include(x => x.quotation.serviceProvider).Include(x => x.bookingServiceProviderDocsLine).FirstOrDefault(x => x.bookingId == id);

            var appUser = _userManager.GetUserAsync(User).Result;

            if (appUser.isServiceProvider)
            {
                if (bookingByQuotation.bookingStatus == BookingStatus.New)
                {
                    bookingByQuotation.bookingStatus = BookingStatus.Ongoing;
                    _context.SaveChanges();
                }
            }
           

            var booking = new Booking();
            booking.bookingStatus = bookingByQuotation.bookingStatus;
            if (bookingByQuotation != null)
            {
                //booking.quotation.enquiry.enquiryLine = bookingByQuotation.quotation.enquiry.enquiryLine;
                booking.quotation = bookingByQuotation.quotation;
                booking.bookingId = bookingByQuotation.bookingId;
                booking.quotationId = bookingByQuotation.quotationId;
                booking.customer = bookingByQuotation.customer;
                booking.customerId = bookingByQuotation.customerId;
                booking.serviceProvider = bookingByQuotation.quotation.serviceProvider;
                booking.serviceProvider.companyName = bookingByQuotation.quotation.serviceProvider.companyName;
                booking.bookingNumber = bookingByQuotation.bookingNumber;
                booking.etd = bookingByQuotation.etd;
                booking.eta = bookingByQuotation.eta;
                booking.transitDays = bookingByQuotation.transitDays;
                booking.expeditionType = bookingByQuotation.expeditionType;
                booking.shipmentTrackingLinks = bookingByQuotation.shipmentTrackingLinks;
                booking.trackingLink = bookingByQuotation.trackingLink;
                booking.trackingReferenceNo = bookingByQuotation.trackingReferenceNo;
                booking.terms = bookingByQuotation.terms;
                booking.additionalInformation = bookingByQuotation.additionalInformation;
                booking.bookingChargesLine = bookingByQuotation.bookingChargesLine;
                booking.bookingServiceProviderDocsLine = bookingByQuotation.bookingServiceProviderDocsLine;

            }
     
            //booking.originOfShipmentLocationName = bookingByQuotation.originOfShipment.locationName;
            //booking.destinationOfShipmentLocationName = bookingByQuotation.originOfShipment.locationName;

            

            return View(booking);
        }


        [HttpGet]
        [Authorize]
        public IActionResult GetServiceProviderDocsLine(string masterid)
        {
            var list = _context.BookingServiceProviderDocsLine.Where(x => x.bookingId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }


        [HttpGet]
        [Authorize]
        public IActionResult GetCustomerDocsLine(string masterid)
        {
            var list = _context.BookingCustomerDocsLine.Where(x => x.bookingId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        //Request Doc From Service Provider To Customer
        [HttpGet]
        public async Task<IActionResult> RequestForDocumentsServiceProvider(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.Include(x => x.quotation).Include(x => x.customer).Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).SingleOrDefaultAsync(m => m.bookingId == id);

            if (booking == null)
            {
                return NotFound();
            }
            return View(booking);

        }

        [HttpPost]
        public async Task<IActionResult> RequestForDocumentsServiceProvider(string bookingId, string toCustomerId, DocumentType documentType)
        {
            try
            {
                var customerUserId = _context.Customer.FirstOrDefault(x => x.customerId == toCustomerId).applicationUserId;
                var notification = new Notifications();
                notification.Name = NotificationNameModel.DocumentRequestNotification;
                notification.fromUserId = _userManager.GetUserId(User);
                notification.toUserId = customerUserId;  //To Customer
                notification.Link = NotificationLinkModel.DocumentRequest;

                var fromServiceProviderName = _context.ServiceProvider.FirstOrDefault(x => x.applicationUserId == notification.fromUserId).businessContactName;
                var bookingDetails = _context.Booking.FirstOrDefault(x => x.bookingId == bookingId);
                notification.Data = documentType.GetDisplayName() + "," + "Shipper - " + fromServiceProviderName + "," + "Booking No - " + bookingDetails.bookingNumber;
                await _netcoreService.SendUserNotification(notification);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Notification Sent To Customer Successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Notification Could Not Be Sent" });
            }

        }





        //Request Doc from Customer To Service Provider
        [HttpGet]
        public async Task<IActionResult> RequestForDocumentsCustomer(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.Include(x => x.quotation.serviceProvider).Include(x => x.quotation).Include(x => x.customer).Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).SingleOrDefaultAsync(m => m.bookingId == id);

            if (booking == null)
            {
                return NotFound();
            }
            return View(booking);

        }

        [HttpPost]
        public async Task<IActionResult> RequestForDocumentsCustomer(string bookingId, string toServiceProviderId, DocumentType documentType)
        {
            try
            {
                var serviceProviderId = _context.ServiceProvider.FirstOrDefault(x => x.serviceProviderId == toServiceProviderId).applicationUserId;
                var notification = new Notifications();
                notification.Name = NotificationNameModel.DocumentRequestNotification;
                notification.fromUserId = _userManager.GetUserId(User);
                notification.toUserId = serviceProviderId;  //To Service Provider
                notification.Link = NotificationLinkModel.DocumentRequest;

                var fromCustomerName = _context.Customer.FirstOrDefault(x => x.applicationUserId == notification.fromUserId).businessContactName;
                var bookingDetails = _context.Booking.FirstOrDefault(x => x.bookingId == bookingId);
                notification.Data = documentType.GetDisplayName() + "," + "Customer - " + fromCustomerName + "," + "Booking No - " + bookingDetails.bookingNumber;
                await _netcoreService.SendUserNotification(notification);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Notification Sent To Shipper Successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Notification Could Not Be Sent" });
            }

        }

        //Upload to database
        //[HttpPost]
        public async Task<IActionResult> UploadToDatabase(List<IFormFile> data, string bookingId, string serviceProviderId, DocumentType documentType)
        {
            try
            {
                var model = await _netcoreService.UploadFileOnDatabase(data);
                var fileModel = new BookingServiceProviderDocsLine
                {
                    bookingId = bookingId,
                    serviceProviderId = serviceProviderId,
                    fileType = model.fileType,
                    extension = model.extension,
                    documentName = model.name,
                    documentType = documentType,
                    data = model.data
                };
                _context.BookingServiceProviderDocsLine.Add(fileModel);
                _context.SaveChanges();
                //TempData["Message"] = "File successfully uploaded to Database";
                //return Ok(model.name);
                return Json(new { success = true, message = "File Uploaded Successfully" });


            }
            catch (Exception ex)
            {
                //TempData["Message"] = "File not uploaded to Database";
                //return StatusCode(500, new { message = ex.Message });
                return Json(new { success = false, message = "File Upload Not Successfull" });
            }

        }


        public async Task<IActionResult> UploadToDatabaseCustomer(List<IFormFile> data, string bookingId, string customerId, DocumentType documentType)
        {
            try
            {
                var model = await _netcoreService.UploadFileOnDatabase(data);
                var fileModel = new BookingCustomerDocsLine
                {
                    bookingId = bookingId,
                    customerId = customerId,
                    fileType = model.fileType,
                    extension = model.extension,
                    documentName = model.name,
                    documentType = documentType,
                    data = model.data
                };
                _context.BookingCustomerDocsLine.Add(fileModel);
                _context.SaveChanges();
                //TempData["Message"] = "File successfully uploaded to Database";
                //return Ok(model.name);
                return Json(new { success = true, message = "File Uploaded Successfully" });


            }
            catch (Exception ex)
            {
                //TempData["Message"] = "File not uploaded to Database";
                //return StatusCode(500, new { message = ex.Message });
                return Json(new { success = false, message = "File Upload Not Successfull" });
            }

        }


        public async Task<IActionResult> DownloadFileFromDatabase(string id)
        {
            var file = await _context.BookingServiceProviderDocsLine.Where(x => x.bookingServiceProviderDocsLineId == id).FirstOrDefaultAsync();
            if (file == null) return null;
            return File(file.data, file.fileType, file.documentName + file.extension);
        }

    
        public async Task<IActionResult> DeleteFileFromDatabase(string id)
        {

            var file = await _context.BookingServiceProviderDocsLine.Where(x => x.bookingServiceProviderDocsLineId == id).FirstOrDefaultAsync();
            _context.BookingServiceProviderDocsLine.Remove(file);
            _context.SaveChanges();
            TempData["Message"] = $"Removed {file.documentName + file.extension} successfully from Database.";

            return Json(new { success = true, message = "Document Information Deletion Successful" });
        }


        public async Task<IActionResult> DownloadCustomerFileFromDatabase(string id)
        {
            var file = await _context.BookingCustomerDocsLine.Where(x => x.bookingCustomerDocsLineId == id).FirstOrDefaultAsync();
            if (file == null) return null;
            return File(file.data, file.fileType, file.documentName + file.extension);
        }


        public async Task<IActionResult> DeleteCustomerFileFromDatabase(string id)
        {

            var file = await _context.BookingCustomerDocsLine.Where(x => x.bookingCustomerDocsLineId == id).FirstOrDefaultAsync();
            _context.BookingCustomerDocsLine.Remove(file);
            _context.SaveChanges();
            TempData["Message"] = $"Removed {file.documentName + file.extension} successfully from Database.";

            return Json(new { success = true, message = "Document Information Deletion Successful" });
        }


        public IActionResult BookingView(string bookingId)
        {
            var bookingView = new BookingViewModel();
            var bookingChargesLineView = new List<BookingChargesLineViewModel>();

            if (bookingId != null)
            {
                var bookingModel = _context.Booking.FirstOrDefault(x => x.bookingId == bookingId);

                var bookingChargesLine = _context.BookingChargesLine.Where(x => x.bookingId == bookingId).ToList();
            }


            //foreach (var item in bookingChargesLine)
            //{
            //    var model = new BookingChargesLineViewModel();

            //    bookingChargesLine.Add(model);

            //}



            return PartialView("BookingView", bookingView);
        }



        // GET: ServiceProvider/Edit/5
        public async Task<IActionResult> BookingShipmentTrackingEdit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.Include(x => x.shipmentTrackingLinks).Include(x => x.quotation).Include(x => x.customer).Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).SingleOrDefaultAsync(m => m.bookingId == id);
            ViewData["shipmentTrackingLinksId"] = new SelectList(_context.ShipmentTrackingLinks.ToList(), "shipmentTrackingId", "shipmentName", booking.shipmentTrackingId);

            if (booking == null)
            {
                return NotFound();
            }
            return View(booking);
        }

        // POST: Vendor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BookingShipmentTrackingEdit(string id, Booking booking)
        {
            if (id != booking.bookingId)
            {
                return NotFound();
            }
            ViewData["shipmentTrackingLinksId"] = new SelectList(_context.ShipmentTrackingLinks.ToList(), "shipmentTrackingId", "shipmentName", booking.shipmentTrackingId);

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.bookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(booking);
        }


        // GET: Booking/Create
        //public IActionResult Create(string id) //Enquiry id
        //{
        //    ViewData["StatusMessage"] = TempData["StatusMessage"];
        //    List<Enquiry> enquiryList = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.enquiryExpiryDate >= DateTime.Now).ToList();
        //    enquiryList.Insert(0, new Enquiry { enquiryId = "0", enquiryNumber = "Select" });
        //    ViewData["enquiryId"] = new SelectList(enquiryList, "enquiryId", "enquiryNumber", id);


        //    //From Enquiry
        //    List<Location> locationList = _context.Location.ToList();
        //    var originLocationId = enquiryList.FirstOrDefault(x => x.enquiryId == id).originOfShipmentId;
        //    ViewData["originOfShipmentId"] = new SelectList(locationList, "locationId", "locationName", originLocationId);
        //    var destinationLocationId = enquiryList.FirstOrDefault(x => x.enquiryId == id).destinationOfShipmentId;
        //    ViewData["destinationOfShipmentId"] = new SelectList(locationList, "locationId", "locationName", destinationLocationId);
        //    var modeOfTransport = enquiryList.FirstOrDefault(x => x.enquiryId == id).modeOfTransport;


        //    Booking booking = new Booking();
        //    booking.enquiryId = id;
        //    booking.modeOfTransport = modeOfTransport;
        //    booking.originOfShipmentId = originLocationId.Value;
        //    booking.destinationOfShipmentId = destinationLocationId.Value;

        //    //var bookingData = _context.Booking.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).FirstOrDefault(x => x.enquiryId == id);

        //    //if (bookingData != null)
        //    //{
        //    //    booking.terms = bookingData.terms;
        //    //    booking.additionalInformation = bookingData.additionalInformation;
        //    //}
        //    booking.bookingStatus = BookingStatus.Ongoing;
        //    return View(booking);
        //}


        //public async Task<IActionResult> BookNow(Booking booking)
        //{

        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Finish(string id, bool isFinished)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            var booking = _context.Booking.Include(x => x.quotation.enquiry).FirstOrDefault(m => m.bookingId == id);
            var enquirySentUserId = _context.ApplicationUser.FirstOrDefault(x => x.UserName == booking.quotation.enquiry.userName).Id;

            if (isFinished)
            {
                booking.bookingStatus = BookingStatus.Closed;
                booking.quotation.enquiry.enquiryStatus = EnquiryStatus.Closed;
                _context.Booking.Update(booking);
                _context.SaveChanges();

                var notification = new Notifications();
                notification.Name = NotificationNameModel.CompletedNotification;
                notification.fromUserId = _userManager.GetUserId(User);
                notification.toUserId = enquirySentUserId;
                notification.Link = NotificationLinkModel.BookingsLink;
                notification.Data = NotificationMessageModel.ShipmentCompleteMessage;
                await _netcoreService.SendUserNotification(notification);
                await _context.SaveChangesAsync();
            }

            //return RedirectToAction(nameof(Index));

            return Json(new { success = true });
        }


        //[HttpGet]
        //// GET: Booking/Create/5
        //public IActionResult Create(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["StatusMessage"] = TempData["StatusMessage"];
        //    List<Enquiry> enquiryList = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.enquiryExpiryDate >= DateTime.Now).ToList();

        //    var enquiryById = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).FirstOrDefault(m => m.enquiryId == id);
        //    //enquiryList.Insert(0, new Enquiry { enquiryId = "0", enquiryNumber = "Select" });
        //    ViewData["enquiryId"] = new SelectList(enquiryList, "enquiryId", "enquiryNumber", id);
        //    ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiryById.originOfShipmentId);
        //    ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiryById.destinationOfShipmentId);
        //    Booking booking = new Booking();
        //    return View(booking);
        //}




        // POST: Booking/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string quotationId)
        {
            if (quotationId == null)
            {
                TempData["StatusMessage"] = "Error. Booking is not valid. Please select valid quotation";
                return RedirectToAction(nameof(Create));
            }

            if (ModelState.IsValid)
            {
                //check booking
                var check = await _context.Booking.Include(x => x.quotation).SingleOrDefaultAsync(x => x.quotationId.Equals(quotationId));
                if (check != null)
                {
                    ViewData["StatusMessage"] = "Error. Booking already quoted. " + check.quotation.quotationNumber;

                    return View(check);
                }

                var booking = new Booking();

                booking.quotation = await _context.Quotation.Include(x => x.shipmentTrackingLinks).Include(x => x.enquiry.originOfShipment).Include(x => x.enquiry.destinationOfShipment).Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Include(x => x.serviceProvider).SingleOrDefaultAsync(x => x.quotationId.Equals(quotationId));
                var userId = _userManager.GetUserId(User);
                var customerId = _context.Customer.SingleOrDefault(x => x.applicationUserId == userId).customerId;
                booking.originOfShipmentId = booking.quotation.enquiry.originOfShipmentId.Value;
                booking.destinationOfShipmentId = booking.quotation.enquiry.destinationOfShipmentId.Value;
                booking.shipmentTrackingId = booking.quotation.shipmentTrackingId;
                booking.trackingLink = booking.quotation.shipmentTrackingLinks.shipmentTrackingLink;
                booking.customerId = customerId;
                booking.bookingStatus = BookingStatus.New;
                booking.bookingDate = DateTime.Now + DateTime.Now.TimeOfDay;

                _context.Booking.Add(booking);

                //change status of quotation to completed
                booking.quotation.quotationStatus = QuotationStatus.Approved;
                _context.Quotation.Update(booking.quotation);
                await _context.SaveChangesAsync();



                //auto create booking charges line
                var quotationChargesLines = _context.QuotationChargesLine.Where(x => x.quotationId.Equals(quotationId)).ToList();

                foreach (var item in quotationChargesLines)
                {
                    BookingChargesLine bookingLine = new BookingChargesLine();
                    bookingLine.booking = booking;
                    bookingLine.bookingChargesLineId = Guid.NewGuid().ToString();
                    bookingLine.bookingId = booking.bookingId;
                    bookingLine.chargeName = item.chargeName;
                    bookingLine.amount = item.amount;

                    _context.BookingChargesLine.Add(bookingLine);
                    await _context.SaveChangesAsync();

                }


                var quotationProviderUserId = _context.ApplicationUser.FirstOrDefault(x => x.Id == booking.quotation.serviceProvider.applicationUserId).Id;

                var notification = new Notifications();
                notification.Name = NotificationNameModel.BookingNotification;
                notification.fromUserId = _userManager.GetUserId(User);
                notification.toUserId = quotationProviderUserId;
                notification.Link = NotificationLinkModel.BookingsLink;
                notification.Data = NotificationMessageModel.NewBookingMessage;
                await _netcoreService.SendUserNotification(notification);
                await _context.SaveChangesAsync();

                //auto create receiving line, full receive
                //var quotationChargesLines = _context.QuotationChargesLine.Where(x => x.quotationId.Equals(quotationId)).ToList();

                //foreach (var item in quotationChargesLines)
                //{
                //    BookingContainerLine line = new BookingContainerLine();
                //    line.booking = booking;
                //    line.serviceType = item.serviceType;
                //    line.containerType = item.containerType;
                //    line.commodityType = item.commodityType;
                //    line.volume = item.volume;
                //    line.weight = item.weight;
                //    line.size = item.size;
                //    line.containerLength = item.containerLength;
                //    line.containerWidth = item.containerWidth;
                //    line.containerHeight = item.containerHeight;
                //    line.count = item.count;

                //    _context.BookingContainerLine.Add(line);
                //    await _context.SaveChangesAsync();
                //}

                //return RedirectToAction(nameof(Details), new { id = booking.bookingId });
            }
            //ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", booking.enquiryId);

            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", receiving.branchId);
            //ViewData["vendorId"] = new SelectList(_context.ServiceProvider, "vendorId", "vendorName", receiving.vendorId);
            //ViewData["warehouseId"] = new SelectList(_context.Warehouse, "warehouseId", "warehouseName", receiving.warehouseId);
            return Json(new { success = true });
        }

        // GET: Booking/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.SingleOrDefaultAsync(m => m.bookingId == id);
            if (booking == null)
            {
                return NotFound();
            }
            
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", booking.quotationId);
            ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", booking.originOfShipmentId);
            ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", booking.destinationOfShipmentId);

            return View(booking);
        }

        // POST: Receiving/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("bookingId,enquiryId,bookingNumber,enquiryId,bookingDate,bookingExpiryDate,consignee,shipper,terms,modeOfTransport,originOfShipmentId,destinationOfShipmentId,additionalInformation,bookingStatus,HasChild,createdAt")] Booking booking)
        {
            if (id != booking.bookingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.bookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", booking.quotationId);

            return View(booking);
        }

        // GET: Booking/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                    .Include(r => r.quotation)
                    .SingleOrDefaultAsync(m => m.bookingId == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }




        // POST: Booking/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var booking = await _context.Booking
                .Include(x => x.bookingContainerLine)
                .Include(x => x.bookingChargesLine)
                .Include(x => x.quotation)
                .SingleOrDefaultAsync(m => m.bookingId == id);
            try
            {
                _context.BookingContainerLine.RemoveRange(booking.bookingContainerLine);
                _context.BookingChargesLine.RemoveRange(booking.bookingChargesLine);
                _context.Booking.Remove(booking);

                //rollback status to open
                //booking.enquiry.status = PurchaseOrderStatus.Open;

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(booking);
            }
            
        }

        private bool BookingExists(string id)
        {
            return _context.Booking.Any(e => e.bookingNumber == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class Booking
        {
            public const string Controller = "Booking";
            public const string Action = "Index";
            public const string Role = "Booking";
            public const string Url = "/Booking/Index";
            public const string Name = "Booking";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Booking")]
        public bool BookingRole { get; set; } = false;
    }
}



