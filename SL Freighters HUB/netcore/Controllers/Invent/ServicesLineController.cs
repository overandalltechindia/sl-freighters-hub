﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    [Authorize(Roles = "ServicesLine")]
    public class ServicesLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServicesLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ServicesLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ServicesLine.Include(x => x.serviceProvider);
            return View(await applicationDbContext.ToListAsync());
        }        

    // GET: ServiceLine/Details/5
    public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var serviceLine = await _context.ServicesLine
                .Include(v => v.serviceProvider)
                    .SingleOrDefaultAsync(m => m.servicesLineId == id);
        if (serviceLine == null)
        {
            return NotFound();
        }

        return View(serviceLine);
    }


    // GET: ServicesLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.ServicesLine.SingleOrDefault(m => m.servicesLineId == id);
        var selected = _context.ServiceProvider.SingleOrDefault(m => m.serviceProviderId == masterid);
            ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId");
        if (check == null)
        {
            ServicesLine objline = new ServicesLine();
            objline.serviceProvider = selected;
            objline.serviceProviderId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: ServicesLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("serviceLineId,serviceProviderId,serviceType,createdAt")] ServicesLine servicesLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(servicesLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", servicesLine.serviceProvider);

        return View(servicesLine);
    }

    // GET: ServicesLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var servicesLine = await _context.ServicesLine.SingleOrDefaultAsync(m => m.servicesLineId == id);
        if (servicesLine == null)
        {
            return NotFound();
        }
        
        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", servicesLine.serviceProviderId);
        return View(servicesLine);
    }

    // POST: ServicesLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("serviceLineId,serviceProviderId,serviceType,createdAt")] ServicesLine servicesLine)
    {
        if (id != servicesLine.servicesLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(servicesLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServicesLineExists(servicesLine.servicesLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
        
            ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", servicesLine.serviceProviderId);
        return View(servicesLine);
    }

    // GET: ServicesLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var servicesLine = await _context.ServicesLine
                .Include(v => v.serviceProvider)
                .SingleOrDefaultAsync(m => m.servicesLineId == id);
        if (servicesLine == null)
        {
            return NotFound();
        }

        return View(servicesLine);
    }




    // POST: ServicesLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var servicesLine = await _context.ServicesLine.SingleOrDefaultAsync(m => m.servicesLineId == id);
            _context.ServicesLine.Remove(servicesLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool ServicesLineExists(string id)
    {
        return _context.ServicesLine.Any(e => e.servicesLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class ServicesLine
      {
          public const string Controller = "ServicesLine";
          public const string Action = "Index";
          public const string Role = "ServicesLine";
          public const string Url = "/ServicesLine/Index";
          public const string Name = "ServicesLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "ServicesLine")]
      public bool ServicesLineRole { get; set; } = false;
  }
}



