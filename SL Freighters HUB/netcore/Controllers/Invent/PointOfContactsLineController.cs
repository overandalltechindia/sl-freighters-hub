﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    [Authorize(Roles = "PointOfContactsLine")]
    public class PointOfContactsLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PointOfContactsLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PointOfContactsLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PointOfContactsLine.Include(x => x.serviceProvider);
            return View(await applicationDbContext.ToListAsync());
        }        

    // GET: PointOfContactsLine/Details/5
    public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var pointOfContactLine = await _context.PointOfContactsLine
                .Include(v => v.serviceProvider)
                    .SingleOrDefaultAsync(m => m.pointOfContactLineId == id);
        if (pointOfContactLine == null)
        {
            return NotFound();
        }

        return View(pointOfContactLine);
    }


    // GET: PointOfContactsLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.PointOfContactsLine.SingleOrDefault(m => m.pointOfContactLineId == id);
        var selected = _context.ServiceProvider.SingleOrDefault(m => m.serviceProviderId == masterid);
            ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId");
        if (check == null)
        {
            PointOfContactsLine objline = new PointOfContactsLine();
            objline.serviceProvider = selected;
            objline.serviceProviderId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: PointOfContactsLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("pointOfContactLineId,serviceProviderId,name,designation,contact,email,isDefaultPOC,createdAt")] PointOfContactsLine pointOfContactsLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(pointOfContactsLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", pointOfContactsLine.serviceProvider);

        return View(pointOfContactsLine);
    }

    // GET: PointOfContactLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var pointOfContactsLine = await _context.PointOfContactsLine.SingleOrDefaultAsync(m => m.pointOfContactLineId == id);
        if (pointOfContactsLine == null)
        {
            return NotFound();
        }
        
        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", pointOfContactsLine.serviceProviderId);
        return View(pointOfContactsLine);
    }

    // POST: PointOfContactsLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("pointOfContactLineId,serviceProviderId,name,designation,contact,email,isDefaultPOC,createdAt")] PointOfContactsLine pointOfContactsLine)
    {
        if (id != pointOfContactsLine.pointOfContactLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(pointOfContactsLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PointOfContactsLineExists(pointOfContactsLine.pointOfContactLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
        
            ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", pointOfContactsLine.serviceProviderId);
        return View(pointOfContactsLine);
    }

    // GET: PointOfContactsLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var pointOfContactsLine = await _context.PointOfContactsLine
                .Include(v => v.serviceProvider)
                .SingleOrDefaultAsync(m => m.pointOfContactLineId == id);
        if (pointOfContactsLine == null)
        {
            return NotFound();
        }

        return View(pointOfContactsLine);
    }




    // POST: PointOfContactsLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var pointOfContactsLine = await _context.PointOfContactsLine.SingleOrDefaultAsync(m => m.pointOfContactLineId == id);
            _context.PointOfContactsLine.Remove(pointOfContactsLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool PointOfContactsLineExists(string id)
    {
        return _context.PointOfContactsLine.Any(e => e.pointOfContactLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class PointOfContactsLine
      {
          public const string Controller = "PointOfContactsLine";
          public const string Action = "Index";
          public const string Role = "PointOfContactsLine";
          public const string Url = "/PointOfContactsLine/Index";
          public const string Name = "PointOfContactsLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "PointOfContactsLine")]
      public bool PointOfContactsLineRole { get; set; } = false;
  }
}



