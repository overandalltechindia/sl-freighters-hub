﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    [Authorize(Roles = "ServiceProviderBanksLine")]
    public class ServiceProviderBanksLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServiceProviderBanksLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ServiceProviderBanksLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ServiceProviderBanksLine.Include(x => x.serviceProvider);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ServiceProviderBanksLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var serviceProviderBanksLine = await _context.ServiceProviderBanksLine
                .Include(v => v.serviceProvider)
                    .SingleOrDefaultAsync(m => m.serviceProviderBanksLineId == id);
        if (serviceProviderBanksLine == null)
        {
            return NotFound();
        }

        return View(serviceProviderBanksLine);
    }


    // GET: ServiceProviderBanksLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.ServiceProviderBanksLine.SingleOrDefault(m => m.serviceProviderBanksLineId == id);
        var selected = _context.ServiceProvider.SingleOrDefault(m => m.serviceProviderId == masterid);
            ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId");
        if (check == null)
        {
            ServiceProviderBanksLine objline = new ServiceProviderBanksLine();
            objline.serviceProvider = selected;
            objline.serviceProviderId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: serviceProviderBanksLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("serviceProviderBanksLineId,serviceProviderId,name,designation,contact,email,isDefaultPOC,createdAt")] ServiceProviderBanksLine serviceProviderBanksLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(serviceProviderBanksLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", serviceProviderBanksLine.serviceProvider);

        return View(serviceProviderBanksLine);
    }

    // GET: ServiceProviderBanksLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var serviceProviderBanksLine = await _context.ServiceProviderBanksLine.SingleOrDefaultAsync(m => m.serviceProviderBanksLineId == id);
        if (serviceProviderBanksLine == null)
        {
            return NotFound();
        }
        
        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", serviceProviderBanksLine.serviceProviderId);
        return View(serviceProviderBanksLine);
    }

    // POST: ServiceProviderBanksLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("pointOfContactLineId,serviceProviderId,name,designation,contact,email,isDefaultPOC,createdAt")] ServiceProviderBanksLine serviceProviderBanksLine)
    {
        if (id != serviceProviderBanksLine.serviceProviderBanksLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(serviceProviderBanksLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceProviderBanksLineExists(serviceProviderBanksLine.serviceProviderBanksLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
        
            ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "serviceProviderId", serviceProviderBanksLine.serviceProviderId);
        return View(serviceProviderBanksLine);
    }

    // GET: ServiceProviderBanksLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var serviceProviderBanksLine = await _context.ServiceProviderBanksLine
                .Include(v => v.serviceProvider)
                .SingleOrDefaultAsync(m => m.serviceProviderBanksLineId == id);
        if (serviceProviderBanksLine == null)
        {
            return NotFound();
        }

        return View(serviceProviderBanksLine);
    }




    // POST: ServiceProviderBanksLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var serviceProviderBanksLine = await _context.ServiceProviderBanksLine.SingleOrDefaultAsync(m => m.serviceProviderBanksLineId == id);
            _context.ServiceProviderBanksLine.Remove(serviceProviderBanksLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool ServiceProviderBanksLineExists(string id)
    {
        return _context.ServiceProviderBanksLine.Any(e => e.serviceProviderBanksLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class ServiceProviderBanksLine
        {
          public const string Controller = "ServiceProviderBanksLine";
          public const string Action = "Index";
          public const string Role = "ServiceProviderBanksLine";
          public const string Url = "/ServiceProviderBanksLine/Index";
          public const string Name = "ServiceProviderBanksLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "ServiceProviderBanksLine")]
      public bool ServiceProviderBanksLineRole { get; set; } = false;
  }
}



