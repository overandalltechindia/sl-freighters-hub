﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    //[Authorize(Roles = "BookingServiceProviderDocsLine")]
    public class BookingServiceProviderDocsLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingServiceProviderDocsLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BookingServiceProviderDocsLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BookingServiceProviderDocsLine.Include(x => x.booking).Include(x => x.serviceProvider);
            return View(await applicationDbContext.ToListAsync());
        }        


    // GET: BookingServiceProviderDocsLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.BookingServiceProviderDocsLine.SingleOrDefault(m => m.bookingServiceProviderDocsLineId == id);
        var selected = _context.Booking.Include(x => x.serviceProvider).SingleOrDefault(m => m.quotationId == masterid);
        ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
        ViewData["serviceProviderId"] = new SelectList(_context.ServiceProvider, "serviceProviderId", "businessContactName");
        if (check == null)
        {
            BookingServiceProviderDocsLine objline = new BookingServiceProviderDocsLine();
            objline.booking = selected;
            objline.bookingId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }

    }


    // POST: BookingServiceProviderDocsLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(BookingServiceProviderDocsLine bookingServiceProviderDocsLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(bookingServiceProviderDocsLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        return View(bookingServiceProviderDocsLine);
    }


    private bool PointOfContactsLineExists(string id)
    {
        return _context.PointOfContactsLine.Any(e => e.pointOfContactLineId == id);
    }

  }
}





//namespace netcore.MVC
//{
//  public static partial class Pages
//  {
//      public static class PointOfContactsLinePointOfContactsLine
//      {
//          public const string Controller = "BookingServiceProviderDocsLine";
//          public const string Action = "Index";
//          public const string Role = "BookingServiceProviderDocsLine";
//          public const string Url = "/BookingServiceProviderDocsLine/Index";
//          public const string Name = "BookingServiceProviderDocsLine";
//      }
//  }
//}
//namespace netcore.Models
//{
//  public partial class ApplicationUser
//  {
//      [Display(Name = "BookingServiceProviderDocsLine")]
//      public bool BookingServiceProviderDocsLineRole { get; set; } = false;
//  }
//}



