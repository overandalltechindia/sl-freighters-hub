﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    [Authorize(Roles = "QuotationChargesLine")]
    public class QuotationChargesLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuotationChargesLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: QuotationChargesLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.QuotationChargesLine.Include(r => r.quotation);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: QuotationChargesLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotationChargesLine = await _context.QuotationChargesLine
                    .Include(r => r.quotation)
                        .SingleOrDefaultAsync(m => m.quotationChargesLineId == id);
            if (quotationChargesLine == null)
            {
                return NotFound();
            }

            return View(quotationChargesLine);
        }


        // GET: QuotationChargesLine/Create
        public IActionResult Create(string masterid, string id)
        {
            var check = _context.QuotationChargesLine.SingleOrDefault(m => m.quotationChargesLineId == id);
            var selected = _context.Quotation.SingleOrDefault(m => m.quotationId == masterid);
            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber");

            if (check == null)
            {
                QuotationChargesLine objline = new QuotationChargesLine();
                objline.quotation = selected;
                objline.quotationId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: QuotationChargesLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("quotationChargesLineId,quotationId,chargeName,amount,createdAt")] QuotationChargesLine quotationChargesLine)
        {
            
            if (ModelState.IsValid)
            {
                _context.Add(quotationChargesLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber", quotationChargesLine.quotationId);

            return View(quotationChargesLine);
        }

        // GET: QuotationChargesLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotationChargesLine = await _context.QuotationChargesLine.SingleOrDefaultAsync(m => m.quotationChargesLineId == id);
            if (quotationChargesLine == null)
            {
                return NotFound();
            }

            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber", quotationChargesLine.quotationId);
            return View(quotationChargesLine);
        }

        // POST: QuotationChargesLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("quotationChargesLineId,quotationId,chargeName,amount,createdAt")] QuotationChargesLine quotationChargesLine)
        {
            if (id != quotationChargesLine.quotationChargesLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(quotationChargesLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuotationChargesLineExists(quotationChargesLine.quotationChargesLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["quotationId"] = new SelectList(_context.Quotation, "quotationId", "quotationNumber", quotationChargesLine.quotationId);

            return View(quotationChargesLine);
        }

        // GET: QuotationChargesLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotationChargesLine = await _context.QuotationChargesLine
                    .Include(r => r.quotation)
                    .SingleOrDefaultAsync(m => m.quotationChargesLineId == id);
            if (quotationChargesLine == null)
            {
                return NotFound();
            }

            return View(quotationChargesLine);
        }




        // POST: QuotationChargesLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var quotationChargesLine = await _context.QuotationChargesLine.SingleOrDefaultAsync(m => m.quotationChargesLineId == id);
            _context.QuotationChargesLine.Remove(quotationChargesLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuotationChargesLineExists(string id)
        {
            return _context.QuotationChargesLine.Any(e => e.quotationChargesLineId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class QuotationChargesLine
        {
            public const string Controller = "QuotationChargesLine";
            public const string Action = "Index";
            public const string Role = "QuotationChargesLine";
            public const string Url = "/QuotationChargesLine/Index";
            public const string Name = "QuotationChargesLine";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "QuotationChargesLine")]
        public bool QuotationChargesLineRole { get; set; } = false;
    }
}



