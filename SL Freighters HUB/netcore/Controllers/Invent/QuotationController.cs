﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Models.ScreenViewModels;
using Microsoft.AspNetCore.Identity;
using netcore.Models;
using netcore.Services;

namespace netcore.Controllers.Invent
{


    //[Authorize(Roles = "Quotation")]
    public class QuotationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly INetcoreService _netcoreService;


        public QuotationController(ApplicationDbContext context, 
            UserManager<ApplicationUser> userManager,
            INetcoreService netcoreService
            )
        {
            _context = context;
            _userManager = userManager;
            _netcoreService = netcoreService;

        }

        //public IActionResult GetWarehouseByOrder(string purchaseOrderId)
        //{
        //    PurchaseOrder po = _context.PurchaseOrder
        //        .Include(x => x.branch)
        //        .Where(x => x.purchaseOrderId.Equals(purchaseOrderId)).FirstOrDefault();

        //    List<Warehouse> warehouseList = _context.Warehouse.Where(x => x.branchId.Equals(po.branch.branchId)).ToList();
        //    warehouseList.Insert(0, new Warehouse { warehouseId = "0", warehouseName = "Select" });

        //    return Json(new SelectList(warehouseList, "warehouseId", "warehouseName"));
        //}

        //public async Task<IActionResult> ShowGSRN(string id)
        //{
        //    Receiving obj = await _context.Receiving
        //        .Include(x => x.vendor)
        //        .Include(x => x.purchaseOrder)
        //            .ThenInclude(x => x.branch)
        //        .Include(x => x.receivingLine).ThenInclude(x => x.product)
        //        .SingleOrDefaultAsync(x => x.receivingId.Equals(id));
        //    return View(obj);
        //}

        //public async Task<IActionResult> PrintGSRN(string id)
        //{
        //    Receiving obj = await _context.Receiving
        //        .Include(x => x.vendor)
        //        .Include(x => x.purchaseOrder)
        //            .ThenInclude(x => x.branch)
        //        .Include(x => x.receivingLine).ThenInclude(x => x.product)
        //        .SingleOrDefaultAsync(x => x.receivingId.Equals(id));
        //    return View(obj);
        //}

        // GET: Quotation
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Quotation.OrderByDescending(x => x.createdAt).Include(r => r.serviceProvider).Include(r => r.enquiry).ThenInclude(x => x.enquiryLine).Include(r => r.enquiry.originOfShipment).Include(r => r.enquiry.destinationOfShipment).Include(r => r.originOfShipment).Include(r => r.destinationOfShipment).Include(x => x.quotationChargesLine);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Quotation/Details/5
        public IActionResult Details(string id, bool isNewQuote = false)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["StatusMessage"] = TempData["StatusMessage"];
            List<Enquiry> enquiryList = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.enquiryExpiryDate >= DateTime.Now).ToList();

            var enquiryById = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).FirstOrDefault(m => m.enquiryId == id);
            ViewData["enquiryId"] = new SelectList(enquiryList, "enquiryId", "enquiryNumber", id);
            ViewData["shipmentTrackingLinksId"] = new SelectList(_context.ShipmentTrackingLinks, "shipmentTrackingId", "shipmentName");

            var quotation = new Quotation();

            //quotation.quotationId = quotation.quotationId;

            var user = _userManager.GetUserAsync(User).Result;
            var serviceProvider = _context.ServiceProvider.SingleOrDefault(x => x.applicationUserId == user.Id);


            if (user.isServiceProvider)
            {
                if (isNewQuote)
                {
                    //var isNewQuotation = _context.Quotation.Include(x => x.serviceProvider.applicationUser).Include(x => x.quotationChargesLine).Include(x => x.enquiry).Include(x => x.shipmentTrackingLinks).Include(x => x.serviceProvider).ThenInclude(x => x.servicesLine).FirstOrDefault(x => x.enquiryId == id && x.serviceProviderId == serviceProvider.serviceProviderId);
                    var newQuotation = new Quotation();

                    var quotationById = _context.Quotation.Include(x => x.quotationChargesLine).Include(x => x.enquiry).Include(x => x.shipmentTrackingLinks).Include(x => x.serviceProvider).ThenInclude(x => x.servicesLine).FirstOrDefault(x => x.enquiryId == id && x.serviceProviderId == serviceProvider.serviceProviderId);

                    if (quotationById != null)
                    {
                        newQuotation.quotationId = quotationById.quotationId;
                        newQuotation.quotationNumber = quotationById.quotationNumber;
                        newQuotation.etd = quotationById.etd;
                        newQuotation.eta = quotationById.eta;
                        newQuotation.transitDays = quotationById.transitDays;
                        newQuotation.expeditionType = quotationById.expeditionType;
                        newQuotation.shipmentTrackingId = quotationById.shipmentTrackingLinks.shipmentTrackingId;
                        newQuotation.terms = quotationById.terms;
                        newQuotation.additionalInformation = quotationById.additionalInformation;
                        newQuotation.quotationChargesLine = quotationById.quotationChargesLine;
                  
                        newQuotation.serviceProviderId = serviceProvider.serviceProviderId;
                        newQuotation.serviceProvider = serviceProvider;
                        newQuotation.enquiry = quotationById.enquiry;
                        newQuotation.enquiryId = id;
                        newQuotation.serviceProvider = quotationById.serviceProvider;
                        newQuotation.quotationStatus = quotationById.quotationStatus;

                    }
                    else
                    {
                        newQuotation.quotationId = newQuotation.quotationId;
                        newQuotation.enquiry = enquiryById;
                        newQuotation.enquiryId = id;
                        newQuotation.enquiry.enquiryStatus = enquiryById.enquiryStatus;

                        if (enquiryById.modeOfTransport == ModeOfTransport.Road)
                        {
                            newQuotation.originPickUpAddressPincode = enquiryById.pickupAddressPincode;
                            newQuotation.destinationAddressPincode = enquiryById.deliveryAddressPincode;
                            newQuotation.originOfShipmentLocationName = enquiryById.originOfShipment.locationName;
                            newQuotation.destinationOfShipmentLocationName = enquiryById.destinationOfShipment.locationName;
                        }
                        else if (enquiryById.modeOfTransport == ModeOfTransport.Rail)
                        {
                            newQuotation.originPickUpAddressPincode = enquiryById.pickupAddressPincode;
                            newQuotation.destinationAddressPincode = enquiryById.deliveryAddressPincode;
                        }
                        else
                        {
                            newQuotation.originOfShipmentLocationName = enquiryById.originOfShipment.locationName;
                            newQuotation.destinationOfShipmentLocationName = enquiryById.destinationOfShipment.locationName;
                        }
                    }

                     return View(newQuotation);
                    
                }
               


                else
                {

                    var quotationById = _context.Quotation.Include(x => x.quotationChargesLine).Include(x => x.enquiry).Include(x => x.shipmentTrackingLinks).Include(x => x.serviceProvider).ThenInclude(x => x.servicesLine).FirstOrDefault(x => x.quotationId == id);

                    //Edit Mode when in Draft only
                    if (quotationById != null)
                    {
                        var quotationEdit = new Quotation();

                        //quotation.enquiry = enquiryById;
                        quotationEdit.quotationId = quotationById.quotationId;
                        quotationEdit.quotationNumber = quotationById.quotationNumber;
                        quotationEdit.etd = quotationById.etd;
                        quotationEdit.eta = quotationById.eta;
                        quotationEdit.transitDays = quotationById.transitDays;
                        quotationEdit.expeditionType = quotationById.expeditionType;
                        quotationEdit.shipmentTrackingId = quotationById.shipmentTrackingLinks.shipmentTrackingId;
                        quotationEdit.terms = quotationById.terms;
                        quotationEdit.additionalInformation = quotationById.additionalInformation;
                        quotationEdit.quotationChargesLine = quotationById.quotationChargesLine;
                    
                        quotationEdit.serviceProviderId = serviceProvider.serviceProviderId;
                        quotationEdit.serviceProvider = serviceProvider;
                        quotationEdit.enquiry = quotationById.enquiry;
                        quotationEdit.enquiryId = id;
                        quotationEdit.serviceProvider = quotationById.serviceProvider;
                        quotationEdit.quotationStatus = quotationById.quotationStatus;

                        if (quotationById.enquiry.modeOfTransport == ModeOfTransport.Road)
                        {
                            quotationEdit.originPickUpAddressPincode = quotationById.enquiry.pickupAddressPincode;
                            quotationEdit.destinationAddressPincode = quotationById.enquiry.deliveryAddressPincode;
                            quotationEdit.originOfShipmentLocationName = quotationById.enquiry.originOfShipment.locationName;
                            quotationEdit.destinationOfShipmentLocationName = quotationById.enquiry.destinationOfShipment.locationName;
                        }
                        else if (quotationById.enquiry.modeOfTransport == ModeOfTransport.Rail)
                        {
                            quotationEdit.originPickUpAddressPincode = quotationById.enquiry.pickupAddressPincode;
                            quotationEdit.destinationAddressPincode = quotationById.enquiry.deliveryAddressPincode;
                        }
                        else
                        {
                            quotationEdit.originOfShipmentLocationName = quotationById.enquiry.originOfShipment.locationName;
                            quotationEdit.destinationOfShipmentLocationName = quotationById.enquiry.destinationOfShipment.locationName;
                        }

                        return View(quotationEdit);
                    }
                    else
                    {
                        quotation.serviceProviderId = serviceProvider.serviceProviderId;
                        quotation.serviceProvider = serviceProvider;
                        quotation.enquiry = enquiryById;
                        quotation.enquiryId = id;

                        if (enquiryById.modeOfTransport == ModeOfTransport.Road)
                        {
                            quotation.originPickUpAddressPincode = enquiryById.pickupAddressPincode;
                            quotation.destinationAddressPincode = enquiryById.deliveryAddressPincode;
                            quotation.originOfShipmentLocationName = enquiryById.originOfShipment.locationName;
                            quotation.destinationOfShipmentLocationName = enquiryById.destinationOfShipment.locationName;
                        }
                        else if (enquiryById.modeOfTransport == ModeOfTransport.Rail)
                        {
                            quotation.originPickUpAddressPincode = enquiryById.pickupAddressPincode;
                            quotation.destinationAddressPincode = enquiryById.deliveryAddressPincode;
                        }
                        else
                        {
                            quotation.originOfShipmentLocationName = enquiryById.originOfShipment.locationName;

                            quotation.destinationOfShipmentLocationName = enquiryById.destinationOfShipment.locationName;
                        }
                    }
                }
            }
            else if (user.isSuperAdmin)
            {
                var quotationById = _context.Quotation.Include(x => x.quotationChargesLine).Include(x => x.enquiry).Include(x => x.shipmentTrackingLinks).Include(x => x.serviceProvider).ThenInclude(x => x.servicesLine).FirstOrDefault(x => x.quotationId == id);

                quotation.enquiry = quotationById.enquiry;
                quotation.quotationNumber = quotationById.quotationNumber;
                quotation.enquiryId = quotationById.enquiry.enquiryId;

                quotation.quotationId = quotationById.quotationId;
                quotation.etd = quotationById.etd;
                quotation.eta = quotationById.eta;
                quotation.transitDays = quotationById.transitDays;
                quotation.expeditionType = quotationById.expeditionType;
                quotation.shipmentTrackingId = quotationById.shipmentTrackingLinks.shipmentTrackingId;
                quotation.terms = quotationById.terms;
                quotation.additionalInformation = quotationById.additionalInformation;
                quotation.quotationChargesLine = quotationById.quotationChargesLine;

                quotation.serviceProviderId = quotationById.serviceProviderId;
                quotation.serviceProvider = quotationById.serviceProvider;
                quotation.quotationStatus = quotationById.quotationStatus;


                quotation.quotationChargesLine = quotationById.quotationChargesLine;

                if (quotationById.enquiry.modeOfTransport == ModeOfTransport.Road)
                {
                    quotation.originPickUpAddressPincode = quotationById.enquiry.pickupAddressPincode;
                    quotation.destinationAddressPincode = quotationById.enquiry.deliveryAddressPincode;
                    quotation.originOfShipmentLocationName = quotationById.enquiry.originOfShipment.locationName;
                    quotation.destinationOfShipmentLocationName = quotationById.enquiry.destinationOfShipment.locationName;
                }
                else if (quotationById.enquiry.modeOfTransport == ModeOfTransport.Rail)
                {
                    quotation.originPickUpAddressPincode = quotationById.enquiry.pickupAddressPincode;
                    quotation.destinationAddressPincode = quotationById.enquiry.deliveryAddressPincode;
                }
                else
                {
                    quotation.originOfShipmentLocationName = quotationById.enquiry.originOfShipment.locationName;

                    quotation.destinationOfShipmentLocationName = quotationById.enquiry.destinationOfShipment.locationName;
                }
            }

            return View(quotation);
        }


        //View Quotation
        public async Task<IActionResult> QuotationView(string quotationId)
        {
            var quotationView = new QuotationViewModel();
            var quotationChargesLineView = new List<QuotationChargesLineViewModel>();

            if (quotationId != null)
            {
                var quotationModel = _context.Quotation
                    .Include(x => x.originOfShipment)
                    .Include(x => x.shipmentTrackingLinks)
                    .Include(x => x.destinationOfShipment)
                    .Include(x => x.enquiry.originOfShipment)
                    .Include(x => x.enquiry.destinationOfShipment)
                    .Include(x => x.enquiry).ThenInclude(x => x.enquiryLine)
                    .SingleOrDefault(x => x.quotationId == quotationId);

                var quotationChargesLine = _context.QuotationChargesLine.Include(x => x.quotation).Where(x => x.quotationId == quotationId).ToList();

                quotationView.enquiry = quotationModel.enquiry;
                quotationView.quotationId = quotationModel.quotationId;
                quotationView.consignee = quotationModel.consignee;
                quotationView.shipmentName = quotationModel.shipmentTrackingLinks.shipmentName;
                quotationView.originOfShipmentLocationName = quotationModel.enquiry.originOfShipment.locationName;
                quotationView.destinationOfShipmentLocationName = quotationModel.enquiry.destinationOfShipment.locationName;
                quotationView.etd = quotationModel.etd;
                quotationView.eta = quotationModel.eta;
                quotationView.expeditionType = quotationModel.expeditionType;
                quotationView.transitDays = quotationModel.transitDays;

                foreach (var item in quotationChargesLine)
                {
                    var quotationCharges = new QuotationChargesLineViewModel();
                    quotationCharges.quotationChargesLineId = item.quotationChargesLineId;
                    quotationCharges.chargeName = item.chargeName;
                    quotationCharges.amount = item.amount;

                    quotationView.quotationChargesLine.Add(quotationCharges);

                }

                await _context.SaveChangesAsync();

            }

            return PartialView("QuotationView", quotationView);
        }


        // Create Quotation
        public IActionResult Create(string id) //Enquiry id
        {
            ViewData["StatusMessage"] = TempData["StatusMessage"];
            List<Enquiry> enquiryList = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.enquiryExpiryDate >= DateTime.Now).ToList();
            enquiryList.Insert(0, new Enquiry { enquiryId = "0", enquiryNumber = "Select" });
            ViewData["enquiryId"] = new SelectList(enquiryList, "enquiryId", "enquiryNumber", id);
            ViewData["shipmentTrackingLinksId"] = new SelectList(_context.ShipmentTrackingLinks.ToList(), "shipmentTrackingId", "shipmentName");


            //From Enquiry
            List<Location> locationList = _context.Location.ToList();
            var originLocationId = enquiryList.FirstOrDefault(x => x.enquiryId == id).originOfShipmentId;

            var destinationLocationId = enquiryList.FirstOrDefault(x => x.enquiryId == id).destinationOfShipmentId;


            ViewData["originOfShipmentId"] = new SelectList(_context.Location.ToList()
                                     .Select(u => new
                                     {
                                         locationId = u.locationId,
                                         locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
                                     }),
                                         "locationId", "locationName", originLocationId);
            ViewData["destinationOfShipmentId"] = new SelectList(_context.Location.ToList()
                                         .Select(u => new
                                         {
                                             locationId = u.locationId,
                                             locationName = String.Concat(u.locationName + " (" + u.countryProvince + ")")
                                         }),
                                             "locationId", "locationName", destinationLocationId);

            var modeOfTransport = enquiryList.FirstOrDefault(x => x.enquiryId == id).modeOfTransport;
            var pickupAddressPincode = enquiryList.FirstOrDefault(x => x.enquiryId == id).pickupAddressPincode;
            var deliveryAddressPincode = enquiryList.FirstOrDefault(x => x.enquiryId == id).deliveryAddressPincode;


            Quotation quotation = new Quotation();
            quotation.enquiryId = id;
            //var userId = _userManager.GetUserId(User);
            var userId = _context.ApplicationUser.SingleOrDefault(x => x.UserName == User.Identity.Name).Id;
            var serviceProviderId = _context.ServiceProvider.SingleOrDefault(x => x.applicationUserId == userId).serviceProviderId;
            quotation.serviceProviderId = serviceProviderId;
            quotation.modeOfTransport = modeOfTransport;

            if (quotation.modeOfTransport == ModeOfTransport.Road)
            {
                quotation.originOfShipmentId = originLocationId.Value;
                quotation.destinationOfShipmentId = destinationLocationId.Value;
                quotation.originPickUpAddressPincode = pickupAddressPincode;
                quotation.destinationAddressPincode = deliveryAddressPincode;
            }
            else if (quotation.modeOfTransport == ModeOfTransport.Rail)
            {
                quotation.originPickUpAddressPincode = pickupAddressPincode;
                quotation.destinationAddressPincode = deliveryAddressPincode;
            }
            else
            {
                quotation.originOfShipmentId = originLocationId.Value;
                quotation.destinationOfShipmentId = destinationLocationId.Value;
            }

            _context.SaveChanges();
            //var quotationData = _context.Quotation.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).FirstOrDefault(x => x.enquiryId == id);

            //if (quotationData != null)
            //{
            //    quotation.terms = quotationData.terms;
            //    quotation.additionalInformation = quotationData.additionalInformation;
            //}
            quotation.quotationStatus = QuotationStatus.Draft;
            return View(quotation);
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Finish(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotation = _context.Quotation.Include(x => x.enquiry).FirstOrDefault(m => m.quotationId == id);
            var enquirySentUserId = _context.ApplicationUser.FirstOrDefault(x => x.UserName == quotation.enquiry.userName).Id;

            if (quotation != null)
            {
                quotation.quotationStatus = QuotationStatus.Submitted;
                quotation.userName = User.Identity.Name;
                quotation.enquiry.enquiryStatus = EnquiryStatus.Accepted;
                _context.Quotation.Update(quotation);
                _context.SaveChanges();


                if (enquirySentUserId != null)
                {
                    var notification = new Notifications();
                    notification.Name = NotificationNameModel.QuotationNotification;
                    notification.fromUserId = _userManager.GetUserId(User);
                    notification.toUserId = enquirySentUserId;
                    notification.Link = NotificationLinkModel.QuotationsLink;
                    notification.Data = NotificationMessageModel.NewQuotationMessage;
                    await _netcoreService.SendUserNotification(notification);
                    await _context.SaveChangesAsync();
                }

            }

            //return RedirectToAction(nameof(Index));

            return Json(new { success = true });
        }


        //[HttpGet]
        //// GET: Quotation/Create/5
        //public IActionResult Create(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["StatusMessage"] = TempData["StatusMessage"];
        //    List<Enquiry> enquiryList = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).Where(x => x.enquiryExpiryDate >= DateTime.Now).ToList();

        //    var enquiryById = _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).FirstOrDefault(m => m.enquiryId == id);
        //    //enquiryList.Insert(0, new Enquiry { enquiryId = "0", enquiryNumber = "Select" });
        //    ViewData["enquiryId"] = new SelectList(enquiryList, "enquiryId", "enquiryNumber", id);
        //    ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiryById.originOfShipmentId);
        //    ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", enquiryById.destinationOfShipmentId);
        //    Quotation quotation = new Quotation();
        //    return View(quotation);
        //}




        // POST: Quotation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("quotationId,enquiryId,quotationNumber,enquiryId,quotationDate,quotationExpiryDate,consignee,shipper,terms,modeOfTransport,originOfShipmentId,destinationOfShipmentId,additionalInformation,quotationStatus,HasChild,createdAt")] Quotation quotation)
        //{
        //    if (quotation.enquiryId == "0")
        //    {
        //        TempData["StatusMessage"] = "Error. Enquiry is not valid. Please select valid enquiry";
        //        return RedirectToAction(nameof(Create));
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        //check quotation
        //        var check = await _context.Quotation.SingleOrDefaultAsync(x => x.enquiryId.Equals(quotation.enquiryId));
        //        if (check != null)
        //        {
        //            ViewData["StatusMessage"] = "Error. Enquiry already quoted. " + check.quotationNumber;

        //            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", quotation.enquiryId);
        //            ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", quotation.originOfShipmentId);
        //            ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", quotation.destinationOfShipmentId);
        //            //ViewData["vendorId"] = new SelectList(_context.ServiceProvider, "vendorId", "vendorName");
        //            //ViewData["warehouseId"] = new SelectList(_context.Warehouse, "warehouseId", "warehouseName");

        //            return View(quotation);
        //        }
        //        quotation.enquiry = await _context.Enquiry.Include(x => x.originOfShipment).Include(x => x.destinationOfShipment).SingleOrDefaultAsync(x => x.enquiryId.Equals(quotation.enquiryId));
        //        //receiving.vendor = receiving.purchaseOrder.vendor;

        //        _context.Add(quotation);

        //        //change status of enquiry to completed
        //        //receiving.purchaseOrder.purchaseOrderStatus = PurchaseOrderStatus.Completed;
        //        //_context.PurchaseOrder.Update(receiving.purchaseOrder);

        //        await _context.SaveChangesAsync();

        //        //auto create receiving line, full receive
        //        List<EnquiryLine> enlines = new List<EnquiryLine>();
        //        enlines = _context.EnquiryLine.Where(x => x.enquiryId.Equals(quotation.enquiryId)).ToList();

        //        foreach (var item in enlines)
        //        {
        //            QuotationContainerLine line = new QuotationContainerLine();
        //            line.quotation = quotation;
        //            line.serviceType = item.serviceType;
        //            line.containerType = item.containerType;
        //            line.commodityType = item.commodityType;
        //            line.volume = item.volume;
        //            line.weight = item.weight;
        //            line.size = item.size;
        //            line.containerLength = item.containerLength;
        //            line.containerWidth = item.containerWidth;
        //            line.containerHeight = item.containerHeight;
        //            line.count = item.count;

        //            _context.QuotationContainerLine.Add(line);
        //            await _context.SaveChangesAsync();
        //        }

        //        return RedirectToAction(nameof(Details), new { id = quotation.quotationId });
        //    }
        //    ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", quotation.enquiryId);

        //    //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", receiving.branchId);
        //    //ViewData["vendorId"] = new SelectList(_context.ServiceProvider, "vendorId", "vendorName", receiving.vendorId);
        //    //ViewData["warehouseId"] = new SelectList(_context.Warehouse, "warehouseId", "warehouseName", receiving.warehouseId);
        //    return View(quotation);
        //}

        // GET: Quotation/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotation = await _context.Quotation.SingleOrDefaultAsync(m => m.quotationId == id);
            if (quotation == null)
            {
                return NotFound();
            }
            
            ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", quotation.enquiryId);
            ViewData["originOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", quotation.originOfShipmentId);
            ViewData["destinationOfShipmentId"] = new SelectList(_context.Location, "locationId", "locationName", quotation.destinationOfShipmentId);
            ViewData["shipmentTrackingLinksId"] = new SelectList(_context.ShipmentTrackingLinks, "shipmentTrackingId", "shipmentName", quotation.shipmentTrackingId);

            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", receiving.branchId);
            //ViewData["vendorId"] = new SelectList(_context.ServiceProvider, "vendorId", "vendorName", receiving.vendorId);
            //ViewData["warehouseId"] = new SelectList(_context.Warehouse, "warehouseId", "warehouseName", receiving.warehouseId);
            return View(quotation);
        }

        // POST: Receiving/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(string id, [Bind("quotationId,enquiryId,quotationNumber,enquiryId,quotationDate,quotationExpiryDate,consignee,shipper,terms,modeOfTransport,originOfShipmentId,destinationOfShipmentId,additionalInformation,quotationStatus,HasChild,createdAt")] Quotation quotation)
        //{
        //    if (id != quotation.quotationId)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(quotation);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!QuotationExists(quotation.quotationId))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["enquiryId"] = new SelectList(_context.Enquiry, "enquiryId", "enquiryNumber", quotation.enquiryId);

        //    //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", receiving.branchId);
        //    //ViewData["vendorId"] = new SelectList(_context.ServiceProvider, "vendorId", "vendorName", receiving.vendorId);
        //    //ViewData["warehouseId"] = new SelectList(_context.Warehouse, "warehouseId", "warehouseName", receiving.warehouseId);
        //    return View(quotation);
        //}

        // GET: Quotation/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var quotation = await _context.Quotation
                    .Include(r => r.enquiry)
                    .SingleOrDefaultAsync(m => m.quotationId == id);
            if (quotation == null)
            {
                return NotFound();
            }

            return View(quotation);
        }




        // POST: Quotation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var quotation = await _context.Quotation
                .Include(x => x.quotationContainerLine)
                .Include(x => x.quotationChargesLine)
                .Include(x => x.enquiry)
                .SingleOrDefaultAsync(m => m.quotationId == id);
            try
            {
                _context.QuotationContainerLine.RemoveRange(quotation.quotationContainerLine);
                _context.QuotationChargesLine.RemoveRange(quotation.quotationChargesLine);
                _context.Quotation.Remove(quotation);

                //rollback status to open
                //quotation.enquiry.status = PurchaseOrderStatus.Open;

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(quotation);
            }
            
        }

        private bool QuotationExists(string id)
        {
            return _context.Quotation.Any(e => e.quotationNumber == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class Quotation
        {
            public const string Controller = "Quotation";
            public const string Action = "Index";
            public const string Role = "Quotation";
            public const string Url = "/Quotation/Index";
            public const string Name = "Quotation";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Quotation")]
        public bool QuotationRole { get; set; } = false;
    }
}



