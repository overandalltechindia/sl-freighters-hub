﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using netcore.Data;
using netcore.Models.Invent;
using netcore.Models;
using Microsoft.AspNetCore.Identity;
//using ReflectionIT.Mvc.Paging;

namespace netcore.Controllers
{
    
    //[Authorize(Roles = "NewsArticle")]
    public class NewsArticleController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public NewsArticleController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: NewsArticle
        public async Task<IActionResult> Index()
        {
            return View(await _context.NewsArticles.Include(x => x.author).ToListAsync());
        }

        // GET: NewsArticle
        public async Task<IActionResult> NewsDisplay(int page = 0)
        {
            var list = _context.NewsArticles.Include(x => x.author).AsNoTracking().OrderByDescending(p => p.postedDate);

            const int PageSize = 3;

            var count = list.Count();

            var data = await list.Skip(page * PageSize).Take(PageSize).ToListAsync();

            ViewBag.MaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);

            ViewBag.Page = page;

            return View(data);
        }


        // GET: NewsArticle/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsArticle = await _context.NewsArticles
                        .SingleOrDefaultAsync(m => m.newsArticlesId == id);
            if (newsArticle == null)
            {
                return NotFound();
            }

            return View(newsArticle);
        }


        // GET: NewsArticle/Create
        public IActionResult Create()
        {
            return View();
        }




        // POST: NewsArticle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(NewsArticles newsArticle)
        {
            if (ModelState.IsValid)
            {
                newsArticle.authorId = _userManager.GetUserId(User);
                if (newsArticle.isPosted)
                {
                    newsArticle.postedDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }
                else
                {
                    newsArticle.postedDate = null;
                }
                newsArticle.createdDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                _context.Add(newsArticle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(newsArticle);
        }

        // GET: NewsArticle/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsArticle = await _context.NewsArticles.SingleOrDefaultAsync(m => m.newsArticlesId == id);
            if (newsArticle == null)
            {
                return NotFound();
            }
            return View(newsArticle);
        }

        // POST: NewsArticle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, NewsArticles newsArticle)
        {
            if (id != newsArticle.newsArticlesId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    newsArticle.authorId = _userManager.GetUserId(User);
                    if (newsArticle.isPosted)
                    {
                        newsArticle.postedDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                    }
                    else
                    {
                        newsArticle.postedDate = null;
                    }
                    newsArticle.createdDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                    _context.Update(newsArticle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NewsArticleExists(newsArticle.newsArticlesId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(newsArticle);
        }

        // GET: NewsArticle/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsArticle = await _context.NewsArticles
                    .SingleOrDefaultAsync(m => m.newsArticlesId == id);
            if (newsArticle == null)
            {
                return NotFound();
            }

            return View(newsArticle);
        }




        // POST: NewsArticle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var newsArticle = await _context.NewsArticles.SingleOrDefaultAsync(m => m.newsArticlesId == id);
            try
            {
                _context.NewsArticles.Remove(newsArticle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(newsArticle);
            }
            
        }

        private bool NewsArticleExists(string id)
        {
            return _context.NewsArticles.Any(e => e.newsArticlesId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class NewsArticle
        {
            public const string Controller = "NewsArticle";
            public const string Action = "Index";
            public const string Role = "NewsArticle";
            public const string Url = "/NewsArticle/Index";
            public const string Name = "NewsArticle";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "NewsArticle")]
        public bool NewsArticleRole { get; set; } = false;
    }
}



