﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using netcore.Data;
using netcore.Models;
using netcore.Models.Invent;
using netcore.Services;

namespace netcore.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/UploadCustomerDocumentController")]
    [Authorize]
    public class UploadCustomerDocumentController : Controller
    {
        private readonly INetcoreService _netcoreService;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public UploadCustomerDocumentController(INetcoreService netcoreService,
            IHostingEnvironment env,
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context)
        {
            _netcoreService = netcoreService;
            _env = env;
            _userManager = userManager;
            _context = context;
        }

        [HttpPost]
        [RequestSizeLimit(5000000)]
        public async Task<IActionResult> PostUploadDocument(List<IFormFile> files, string bookingId)
        {
            try
            {
                var fileName = await _netcoreService.UploadFile(files, _env);
                //try to update the document
                var bookingCustomerDocsLine = _context.BookingCustomerDocsLine.SingleOrDefault(x => x.bookingId == bookingId);
                bookingCustomerDocsLine.documentURL = "/uploads/" + fileName;
                _context.Add(bookingCustomerDocsLine);
                _context.SaveChanges();
                return Ok(fileName);
            }
            catch (Exception ex)
            {

                return StatusCode(500, new { message = ex.Message });
            }


        }
    }
}