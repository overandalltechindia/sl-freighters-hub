﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/EnquiryLine")]
    public class EnquiryLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EnquiryLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/EnquiryLine
        [HttpGet]
        [Authorize]
        public IActionResult GetEnquiryLine(string masterid)
        {
            return Json(new { data = _context.EnquiryLine.Where(x => x.enquiryId.Equals(masterid)).ToList() });
        }

        // POST: api/EnquiryLine
        //[HttpPost]
        //[Authorize]
        //public async Task<IActionResult> PostEnquiryLine(EnquiryLine enquiryLine)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (enquiryLine.enquiryLineId == string.Empty)
        //    {
        //        enquiryLine.enquiryLineId = Guid.NewGuid().ToString();
        //        _context.EnquiryLine.Add(enquiryLine);
        //        await _context.SaveChangesAsync();
        //        return Json(new { success = true, message = "Added New Enquiry Information" });
        //    }
        //    else
        //    {
        //        _context.Update(enquiryLine);
        //        await _context.SaveChangesAsync();
        //        return Json(new { success = true, message = "Edited Enquiry Information" });
        //    }

        //}

        // DELETE: api/EnquiryLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteEnquiryLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var enquiryLine = await _context.EnquiryLine.SingleOrDefaultAsync(m => m.enquiryLineId == id);
            if (enquiryLine == null)
            {
                return NotFound();
            }

            _context.EnquiryLine.Remove(enquiryLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Enquiry Information Deletion Successful" });
        }


        private bool EnquiryLineExists(string id)
        {
            return _context.EnquiryLine.Any(e => e.enquiryLineId == id);
        }


    }

}
