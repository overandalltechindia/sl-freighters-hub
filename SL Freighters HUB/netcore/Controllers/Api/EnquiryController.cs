﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/Enquiry")]
    public class EnquiryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EnquiryController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Enquiry
        [HttpGet]
        [Authorize]
        public IActionResult GetEnquiry(string masterid)
        {
            return Json(new { data = _context.Enquiry.Where(x => x.enquiryId.Equals(masterid)).ToList() });
        }

        // POST: api/Enquiry
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostEnquiry([FromBody] Enquiry enquiry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (enquiry.enquiryId == string.Empty)
            {
                enquiry.enquiryId = Guid.NewGuid().ToString();
                //enquiry.applicationUserId = _context.ApplicationUser.FirstOrDefault(x => x.UserName == User.Identity.Name).Id;
                _context.Enquiry.Add(enquiry);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Enquiry Information" });
            }
            else
            {
                _context.Update(enquiry);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Enquiry Information" });
            }

        }

        // DELETE: api/Enquiry/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteEnquiry([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var enquiry = await _context.Enquiry.SingleOrDefaultAsync(m => m.enquiryId == id);
            if (enquiry == null)
            {
                return NotFound();
            }

            _context.Enquiry.Remove(enquiry);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Enquiry Information Deletion Successful" });
        }


        private bool EnquiryExists(string id)
        {
            return _context.Enquiry.Any(e => e.enquiryId == id);
        }


    }

}
