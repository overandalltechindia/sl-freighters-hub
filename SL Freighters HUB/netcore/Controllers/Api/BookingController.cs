﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/Booking")]
    public class BookingController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Booking
        [HttpGet]
        [Authorize]
        public IActionResult GetBookings(string masterid)
        {
            return Json(new { data = _context.Booking.Where(x => x.bookingId.Equals(masterid)).ToList() });
        }

        // POST: api/Booking
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostBooking([FromBody] Booking booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (booking.bookingId == string.Empty)
            {
                booking.bookingId = Guid.NewGuid().ToString();
                //booking.applicationUserId = _context.ApplicationUser.FirstOrDefault(x => x.UserName == User.Identity.Name).Id;
                _context.Booking.Add(booking);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Booking Information" });
            }
            else
            {
                _context.Update(booking);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Booking Information" });
            }

        }

        // DELETE: api/Booking/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteBooking([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var booking = await _context.Booking.SingleOrDefaultAsync(m => m.bookingId == id);
            if (booking == null)
            {
                return NotFound();
            }

            _context.Booking.Remove(booking);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Service Information Deletion Successful" });
        }


        private bool BookingExists(string id)
        {
            return _context.Booking.Any(e => e.bookingId == id);
        }


    }

}
