﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CustomerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Customer
        [HttpGet]
        [Authorize]
        public IActionResult GetCustomers(string masterid)
        {
            return Json(new { data = _context.Customer.Where(x => x.customerId.Equals(masterid)).ToList() });
        }

        // POST: api/Customer
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostCustomer([FromBody] Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (customer.customerId == string.Empty)
            {
                customer.customerId = Guid.NewGuid().ToString();
                //customer.applicationUserId = _context.ApplicationUser.FirstOrDefault(x => x.UserName == User.Identity.Name).Id;
                _context.Customer.Add(customer);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Customer Information" });
            }
            else
            {
                _context.Update(customer);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Customer Information" });
            }

        }

        // DELETE: api/Customer/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteCustomer([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var customer = await _context.Customer.SingleOrDefaultAsync(m => m.customerId == id);
            if (customer == null)
            {
                return NotFound();
            }

            _context.Customer.Remove(customer);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Service Information Deletion Successful" });
        }


        private bool ServicesProviderExists(string id)
        {
            return _context.Customer.Any(e => e.customerId == id);
        }


    }

}
