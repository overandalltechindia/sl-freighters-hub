﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/QuotationChargesLine")]
    public class QuotationChargesLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuotationChargesLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/QuotationChargesLine
        [HttpGet]
        [Authorize]
        public IActionResult GetQuotationChargesLine(string masterid)
        {
            var list = _context.QuotationChargesLine.Where(x => x.quotationId.Equals(masterid)).ToList();
            return Json(new { data = _context.QuotationChargesLine.Where(x => x.quotationId.Equals(masterid)).ToList() });
        }

        // POST: api/QuotationChargesLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostQuotationChargesLine([FromBody] QuotationChargesLine quotationChargesLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (quotationChargesLine.quotationChargesLineId == string.Empty)
            {
                quotationChargesLine.quotationChargesLineId = Guid.NewGuid().ToString();
                _context.QuotationChargesLine.Add(quotationChargesLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Charges Information" });
            }
            else
            {
                _context.Update(quotationChargesLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Charges Information" });
            }

        }

        // DELETE: api/QuotationChargesLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteQuotationChargesLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var quotationChargesLine = await _context.QuotationChargesLine.SingleOrDefaultAsync(m => m.quotationChargesLineId == id);
            if (quotationChargesLine == null)
            {
                return NotFound();
            }

            _context.QuotationChargesLine.Remove(quotationChargesLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Charges Information Deletion Successful" });
        }


        private bool QuotationChargesLineExists(string id)
        {
            return _context.QuotationChargesLine.Any(e => e.quotationChargesLineId == id);
        }


    }

}
