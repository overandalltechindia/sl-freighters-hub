﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/ServiceProviderBanksLine")]
    public class ServiceProviderBanksLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServiceProviderBanksLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ServiceProviderBanksLine
        [HttpGet]
        [Authorize]
        public IActionResult GetServiceProviderBanksLine(string masterid)
        {
            return Json(new { data = _context.ServiceProviderBanksLine.Where(x => x.serviceProviderId.Equals(masterid)).ToList() });
        }

        // POST: api/ServiceProviderBanksLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostServiceProviderBanksLine([FromBody] ServiceProviderBanksLine serviceProviderBanksLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (serviceProviderBanksLine.serviceProviderBanksLineId == string.Empty)
            {
                serviceProviderBanksLine.serviceProviderBanksLineId = Guid.NewGuid().ToString();
                _context.ServiceProviderBanksLine.Add(serviceProviderBanksLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Bank Information" });
            }
            else
            {
                _context.Update(serviceProviderBanksLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Bank Information" });
            }

        }

        // DELETE: api/ServiceProviderBanksLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteserviceProviderBanksLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceProviderBanksLine = await _context.ServiceProviderBanksLine.SingleOrDefaultAsync(m => m.serviceProviderBanksLineId == id);
            if (serviceProviderBanksLine == null)
            {
                return NotFound();
            }

            _context.ServiceProviderBanksLine.Remove(serviceProviderBanksLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Bank Information Deletion Successful" });
        }


        private bool ServiceProviderBanksLineExists(string id)
        {
            return _context.ServiceProviderBanksLine.Any(e => e.serviceProviderBanksLineId == id);
        }


    }

}
