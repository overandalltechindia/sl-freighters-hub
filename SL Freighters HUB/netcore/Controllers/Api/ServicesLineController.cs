﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/ServicesLine")]
    public class ServicesLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServicesLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ServicesLine
        [HttpGet]
        [Authorize]
        public IActionResult GetServicesLine(string masterid)
        {
            return Json(new { data = _context.ServicesLine.Where(x => x.serviceProviderId.Equals(masterid)).ToList() });
        }

        // POST: api/ServicesLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostServicesLine([FromBody] ServicesLine servicesLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (servicesLine.servicesLineId == string.Empty)
            {
                servicesLine.servicesLineId = Guid.NewGuid().ToString();
                _context.ServicesLine.Add(servicesLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Service Information" });
            }
            else
            {
                _context.Update(servicesLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Service Information" });
            }

        }

        // DELETE: api/ServicesLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteServicesLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var servicesLine = await _context.ServicesLine.SingleOrDefaultAsync(m => m.servicesLineId == id);
            if (servicesLine == null)
            {
                return NotFound();
            }

            _context.ServicesLine.Remove(servicesLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Service Information Deletion Successful" });
        }


        private bool ServicesLineExists(string id)
        {
            return _context.ServicesLine.Any(e => e.servicesLineId == id);
        }


    }

}
