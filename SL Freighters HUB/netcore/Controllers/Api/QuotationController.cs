﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;
using Microsoft.AspNetCore.Identity;
using netcore.Models;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/Quotation")]
    public class QuotationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;


        public QuotationController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager
            )
        {
            _context = context;
            _userManager = userManager;

        }

        // GET: api/Quotation
        [HttpGet]
        [Authorize]
        public IActionResult GetQuotation(string masterid)
        {
            return Json(new { data = _context.Quotation.Include(x => x.serviceProvider).Where(x => x.quotationId.Equals(masterid)).ToList() });
        }

        // POST: api/Quotation
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostQuotation([FromBody] Quotation quotation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (quotation.quotationId == string.Empty)
            {
                quotation.quotationId = Guid.NewGuid().ToString();
                var userId = _userManager.GetUserId(User);
                var serviceProviderId = _context.ServiceProvider.SingleOrDefault(x => x.applicationUserId == userId).serviceProviderId;
                quotation.serviceProviderId = serviceProviderId;
                quotation.quotationDate = DateTime.Now + DateTime.Now.TimeOfDay;
                _context.Quotation.Add(quotation);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Quotation Information" });
            }
            else
            {
                _context.Update(quotation);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Quotation Information" });
            }

        }

        // DELETE: api/Quotation/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteQuotation([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var quotation = await _context.Quotation.SingleOrDefaultAsync(m => m.quotationId == id);
            if (quotation == null)
            {
                return NotFound();
            }

            _context.Quotation.Remove(quotation);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Quotation Information Deletion Successful" });
        }


        private bool QuotationExists(string id)
        {
            return _context.Quotation.Any(e => e.quotationId == id);
        }


    }

}
