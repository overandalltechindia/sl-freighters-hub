﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netcore.Data;
using netcore.Models;
using netcore.Models.Invent;
using netcore.Services;

namespace netcore.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/UploadServiceProviderDocument")]
    [Authorize]
    public class UploadServiceProviderDocumentController : Controller
    {
        private readonly INetcoreService _netcoreService;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public UploadServiceProviderDocumentController(INetcoreService netcoreService,
            IHostingEnvironment env,
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context)
        {
            _netcoreService = netcoreService;
            _env = env;
            _userManager = userManager;
            _context = context;
        }

        //[HttpPost]
        //[RequestSizeLimit(5000000)]
        //public async Task<IActionResult> PostUploadDocument(List<IFormFile> files, string bookingId, string serviceProviderId, DocumentType documentType)
        //{
        //    try
        //    {
        //        var fileName = await _netcoreService.UploadFile(files, _env);
        //        //try to update the document
        //        var bookingServiceProviderDocsLine = new BookingServiceProviderDocsLine();
        //        bookingServiceProviderDocsLine.documentURL = "/uploads/" + fileName;
        //        bookingServiceProviderDocsLine.bookingId = bookingId;
        //        bookingServiceProviderDocsLine.serviceProviderId = serviceProviderId;
        //        bookingServiceProviderDocsLine.documentType = documentType;
        //        _context.Add(bookingServiceProviderDocsLine);
        //        _context.SaveChanges();
        //        return Ok(fileName);
        //    }
        //    catch (Exception ex)
        //    {

        //        return StatusCode(500, new { message = ex.Message });
        //    }


        //}


        ////Upload to database
        //[HttpPost]
        //public async Task<IActionResult> PostUploadDocument(List<IFormFile> files, string bookingId, string serviceProviderId, DocumentType documentType)
        //{
        //    try
        //    {
        //        var model = await _netcoreService.UploadFileOnDatabase(files);
        //        var fileModel = new BookingServiceProviderDocsLine
        //        {
        //            bookingId = bookingId,
        //            serviceProviderId = serviceProviderId,
        //            fileType = model.fileType,
        //            extension = model.extension,
        //            documentName = model.name,
        //            documentType = documentType,
        //            data = model.data
        //        };
        //        _context.BookingServiceProviderDocsLine.Add(fileModel);
        //        _context.SaveChanges();
        //        TempData["Message"] = "File successfully uploaded to Database";
        //        return Ok(model.name);

        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["Message"] = "File not uploaded to Database";
        //        return StatusCode(500, new { message = ex.Message });
        //    }

        //}


        //public async Task<IActionResult> DownloadFileFromDatabase(string id)
        //{
        //    var file = await _context.BookingServiceProviderDocsLine.Where(x => x.bookingServiceProviderDocsLineId == id).FirstOrDefaultAsync();
        //    if (file == null) return null;
        //    return File(file.data, file.fileType, file.documentName + file.extension);
        //}


        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteFileFromDatabase(string id)
        //{

        //    var file = await _context.BookingServiceProviderDocsLine.Where(x => x.bookingServiceProviderDocsLineId == id).FirstOrDefaultAsync();
        //    _context.BookingServiceProviderDocsLine.Remove(file);
        //    _context.SaveChanges();
        //    TempData["Message"] = $"Removed {file.documentName + file.extension} successfully from Database.";
        //    return RedirectToAction("Index");
        //}
    }
}