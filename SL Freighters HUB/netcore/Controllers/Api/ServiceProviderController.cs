﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/ServiceProvider")]
    public class ServiceProviderController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServiceProviderController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ServiceProvider
        [HttpGet]
        [Authorize]
        public IActionResult GetServiceProviders(string masterid)
        {
            return Json(new { data = _context.ServiceProvider.Where(x => x.serviceProviderId.Equals(masterid)).ToList() });
        }

        // POST: api/ServiceProvider
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostServiceProvider([FromBody] ServiceProvider serviceProvider)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (serviceProvider.serviceProviderId == string.Empty)
            {
                serviceProvider.serviceProviderId = Guid.NewGuid().ToString();
                //serviceProvider.applicationUserId = _context.ApplicationUser.FirstOrDefault(x => x.UserName == User.Identity.Name).Id;
                _context.ServiceProvider.Add(serviceProvider);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Service Provider Information" });
            }
            else
            {
                _context.Update(serviceProvider);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Service Provider Information" });
            }

        }

        // DELETE: api/ServiceProvider/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteServiceProvider([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceProvider = await _context.ServiceProvider.SingleOrDefaultAsync(m => m.serviceProviderId == id);
            if (serviceProvider == null)
            {
                return NotFound();
            }

            _context.ServiceProvider.Remove(serviceProvider);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Service Information Deletion Successful" });
        }


        private bool ServicesProviderExists(string id)
        {
            return _context.ServiceProvider.Any(e => e.serviceProviderId == id);
        }


    }

}
