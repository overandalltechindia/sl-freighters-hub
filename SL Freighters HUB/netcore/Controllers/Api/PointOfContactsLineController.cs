﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/PointOfContactsLine")]
    public class PointOfContactsLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PointOfContactsLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/PointOfContactsLine
        [HttpGet]
        [Authorize]
        public IActionResult GetPointOfContactsLine(string masterid)
        {
            return Json(new { data = _context.PointOfContactsLine.Where(x => x.serviceProviderId.Equals(masterid)).ToList() });
        }

        // POST: api/PointOfContactsLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostPointOfContactsLine([FromBody] PointOfContactsLine pointOfContactsLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (pointOfContactsLine.pointOfContactLineId == string.Empty)
            {
                pointOfContactsLine.pointOfContactLineId = Guid.NewGuid().ToString();
                _context.PointOfContactsLine.Add(pointOfContactsLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Point Of Contact Information" });
            }
            else
            {
                _context.Update(pointOfContactsLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Point Of Contact Information" });
            }

        }

        // DELETE: api/PointOfContactsLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeletePointOfContactsLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pointOfContactsLine = await _context.PointOfContactsLine.SingleOrDefaultAsync(m => m.pointOfContactLineId == id);
            if (pointOfContactsLine == null)
            {
                return NotFound();
            }

            _context.PointOfContactsLine.Remove(pointOfContactsLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Point Of Contact Information Deletion Successful" });
        }


        private bool PointOfContactsLineExists(string id)
        {
            return _context.PointOfContactsLine.Any(e => e.pointOfContactLineId == id);
        }


    }

}
