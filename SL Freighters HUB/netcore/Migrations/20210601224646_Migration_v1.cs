﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ApplicationUserRole = table.Column<bool>(nullable: false),
                    BookingRole = table.Column<bool>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    EnquiryLineRole = table.Column<bool>(nullable: false),
                    EnquiryRole = table.Column<bool>(nullable: false),
                    HomeRole = table.Column<bool>(nullable: false),
                    LocationRole = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    PointOfContactsLineRole = table.Column<bool>(nullable: false),
                    QuotationChargesLineRole = table.Column<bool>(nullable: false),
                    QuotationContainerLineRole = table.Column<bool>(nullable: false),
                    QuotationRole = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ServiceProviderBanksLineRole = table.Column<bool>(nullable: false),
                    ServiceProviderRole = table.Column<bool>(nullable: false),
                    ServicesLineRole = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    isServiceProvider = table.Column<bool>(nullable: false),
                    isSuperAdmin = table.Column<bool>(nullable: false),
                    profilePictureUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    locationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(nullable: true),
                    countryProvince = table.Column<string>(nullable: true),
                    locationName = table.Column<string>(maxLength: 256, nullable: false),
                    locationType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.locationId);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentTrackingLinks",
                columns: table => new
                {
                    shipmentTrackingId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    shipmentName = table.Column<string>(nullable: true),
                    shipmentTrackingLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentTrackingLinks", x => x.shipmentTrackingId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    customerId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    applicationUserId = table.Column<string>(nullable: true),
                    businessContactName = table.Column<string>(maxLength: 50, nullable: true),
                    companyName = table.Column<string>(maxLength: 126, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerName = table.Column<string>(maxLength: 50, nullable: true),
                    description = table.Column<string>(maxLength: 126, nullable: true),
                    gstNo = table.Column<string>(maxLength: 15, nullable: true),
                    location = table.Column<string>(maxLength: 256, nullable: true),
                    officialAddress = table.Column<string>(maxLength: 256, nullable: true),
                    panNo = table.Column<string>(maxLength: 10, nullable: true),
                    primaryContact = table.Column<string>(maxLength: 12, nullable: true),
                    primaryEmail = table.Column<string>(maxLength: 30, nullable: true),
                    registrationDate = table.Column<DateTime>(nullable: true),
                    registrationExpDate = table.Column<DateTime>(nullable: true),
                    street2 = table.Column<string>(maxLength: 10, nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.customerId);
                    table.ForeignKey(
                        name: "FK_Customer_AspNetUsers_applicationUserId",
                        column: x => x.applicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    NotificationId = table.Column<string>(maxLength: 38, nullable: false),
                    Data = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    createdDate = table.Column<DateTime>(nullable: false),
                    fromUserId = table.Column<string>(nullable: true),
                    toUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.NotificationId);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_fromUserId",
                        column: x => x.fromUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_toUserId",
                        column: x => x.toUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProvider",
                columns: table => new
                {
                    serviceProviderId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    addressProofDocument = table.Column<string>(nullable: true),
                    applicationUserId = table.Column<string>(nullable: true),
                    businessContactName = table.Column<string>(maxLength: 50, nullable: true),
                    companyName = table.Column<string>(maxLength: 126, nullable: true),
                    countryOfRegistration = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(maxLength: 126, nullable: true),
                    gstNo = table.Column<string>(maxLength: 15, nullable: true),
                    location = table.Column<string>(maxLength: 256, nullable: true),
                    officialAddress = table.Column<string>(maxLength: 256, nullable: true),
                    panNo = table.Column<string>(maxLength: 10, nullable: true),
                    primaryContact = table.Column<string>(maxLength: 12, nullable: true),
                    primaryEmail = table.Column<string>(maxLength: 30, nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProvider", x => x.serviceProviderId);
                    table.ForeignKey(
                        name: "FK_ServiceProvider_AspNetUsers_applicationUserId",
                        column: x => x.applicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry",
                columns: table => new
                {
                    enquiryId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    comments = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    deliveryAddressPincode = table.Column<string>(nullable: true),
                    destinationLocationType = table.Column<int>(nullable: false),
                    destinationOfShipmentId = table.Column<int>(nullable: true),
                    enquiryDate = table.Column<DateTime>(nullable: false),
                    enquiryExpiryDate = table.Column<DateTime>(nullable: true),
                    enquiryNumber = table.Column<string>(maxLength: 125, nullable: false),
                    enquiryStatus = table.Column<int>(nullable: false),
                    hasCargoDelivery = table.Column<bool>(nullable: false),
                    hasCargoInsurance = table.Column<bool>(nullable: false),
                    hasCargoPickup = table.Column<bool>(nullable: false),
                    hasCustomClearance = table.Column<bool>(nullable: false),
                    hasLogisticsTradeFinance = table.Column<bool>(nullable: false),
                    hasPortForwardingFees = table.Column<bool>(nullable: false),
                    insuranceValue = table.Column<string>(nullable: true),
                    modeOfTransport = table.Column<int>(nullable: false),
                    originLocationType = table.Column<int>(nullable: false),
                    originOfShipmentId = table.Column<int>(nullable: true),
                    pickupAddressPincode = table.Column<string>(nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry", x => x.enquiryId);
                    table.ForeignKey(
                        name: "FK_Enquiry_Location_destinationOfShipmentId",
                        column: x => x.destinationOfShipmentId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Enquiry_Location_originOfShipmentId",
                        column: x => x.originOfShipmentId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "CustomerLine",
                columns: table => new
                {
                    customerLineId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(maxLength: 38, nullable: true),
                    fax = table.Column<string>(maxLength: 20, nullable: true),
                    firstName = table.Column<string>(maxLength: 20, nullable: false),
                    gender = table.Column<int>(nullable: false),
                    jobTitle = table.Column<string>(maxLength: 20, nullable: false),
                    lastName = table.Column<string>(maxLength: 20, nullable: false),
                    middleName = table.Column<string>(maxLength: 20, nullable: true),
                    mobilePhone = table.Column<string>(maxLength: 20, nullable: true),
                    nickName = table.Column<string>(maxLength: 20, nullable: true),
                    officePhone = table.Column<string>(maxLength: 20, nullable: true),
                    personalEmail = table.Column<string>(maxLength: 50, nullable: true),
                    salutation = table.Column<int>(nullable: false),
                    userName = table.Column<string>(nullable: true),
                    workEmail = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerLine", x => x.customerLineId);
                    table.ForeignKey(
                        name: "FK_CustomerLine_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PointOfContactsLine",
                columns: table => new
                {
                    pointOfContactLineId = table.Column<string>(maxLength: 38, nullable: false),
                    contact = table.Column<string>(maxLength: 50, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    designation = table.Column<string>(maxLength: 50, nullable: false),
                    email = table.Column<string>(maxLength: 50, nullable: true),
                    isDefaultPOC = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 126, nullable: true),
                    serviceProviderId = table.Column<string>(maxLength: 38, nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PointOfContactsLine", x => x.pointOfContactLineId);
                    table.ForeignKey(
                        name: "FK_PointOfContactsLine_ServiceProvider_serviceProviderId",
                        column: x => x.serviceProviderId,
                        principalTable: "ServiceProvider",
                        principalColumn: "serviceProviderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviderBanksLine",
                columns: table => new
                {
                    serviceProviderBanksLineId = table.Column<string>(maxLength: 38, nullable: false),
                    accountHolderName = table.Column<string>(maxLength: 256, nullable: true),
                    accountNumber = table.Column<string>(maxLength: 50, nullable: true),
                    bankName = table.Column<string>(maxLength: 256, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    ifscCode = table.Column<string>(maxLength: 50, nullable: true),
                    isDefaultBank = table.Column<bool>(nullable: false),
                    serviceProviderId = table.Column<string>(maxLength: 38, nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviderBanksLine", x => x.serviceProviderBanksLineId);
                    table.ForeignKey(
                        name: "FK_ServiceProviderBanksLine_ServiceProvider_serviceProviderId",
                        column: x => x.serviceProviderId,
                        principalTable: "ServiceProvider",
                        principalColumn: "serviceProviderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServicesLine",
                columns: table => new
                {
                    servicesLineId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    serviceProviderId = table.Column<string>(maxLength: 38, nullable: true),
                    serviceType = table.Column<int>(nullable: false),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicesLine", x => x.servicesLineId);
                    table.ForeignKey(
                        name: "FK_ServicesLine_ServiceProvider_serviceProviderId",
                        column: x => x.serviceProviderId,
                        principalTable: "ServiceProvider",
                        principalColumn: "serviceProviderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EnquiryLine",
                columns: table => new
                {
                    enquiryLineId = table.Column<string>(maxLength: 38, nullable: false),
                    closedLoadType = table.Column<int>(nullable: false),
                    commodityType = table.Column<int>(nullable: false),
                    containerHeight = table.Column<decimal>(nullable: true),
                    containerLength = table.Column<decimal>(nullable: true),
                    containerType = table.Column<int>(nullable: false),
                    containerWidth = table.Column<decimal>(nullable: true),
                    count = table.Column<decimal>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    enquiryId = table.Column<string>(maxLength: 38, nullable: false),
                    isClosedBodyTruck = table.Column<bool>(nullable: false),
                    isRefrigerated = table.Column<bool>(nullable: false),
                    loadType = table.Column<int>(nullable: false),
                    packageType = table.Column<int>(nullable: false),
                    serviceType = table.Column<int>(nullable: false),
                    temperature = table.Column<decimal>(nullable: true),
                    userName = table.Column<string>(nullable: true),
                    volume = table.Column<decimal>(nullable: true),
                    weight = table.Column<decimal>(nullable: true),
                    weightUOM = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnquiryLine", x => x.enquiryLineId);
                    table.ForeignKey(
                        name: "FK_EnquiryLine_Enquiry_enquiryId",
                        column: x => x.enquiryId,
                        principalTable: "Enquiry",
                        principalColumn: "enquiryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Quotation",
                columns: table => new
                {
                    quotationId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    additionalInformation = table.Column<string>(nullable: true),
                    consignee = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    destinationOfShipmentId = table.Column<int>(nullable: true),
                    enquiryId = table.Column<string>(maxLength: 38, nullable: false),
                    eta = table.Column<DateTime>(nullable: false),
                    etd = table.Column<DateTime>(nullable: false),
                    expeditionType = table.Column<int>(nullable: false),
                    modeOfTransport = table.Column<int>(nullable: false),
                    originOfShipmentId = table.Column<int>(nullable: true),
                    quotationDate = table.Column<DateTime>(nullable: false),
                    quotationExpiryDate = table.Column<DateTime>(nullable: true),
                    quotationNumber = table.Column<string>(maxLength: 125, nullable: false),
                    quotationStatus = table.Column<int>(nullable: false),
                    serviceProviderId = table.Column<string>(maxLength: 38, nullable: false),
                    shipmentTrackingId = table.Column<int>(maxLength: 38, nullable: false),
                    shipper = table.Column<string>(nullable: true),
                    terms = table.Column<string>(nullable: true),
                    transitDays = table.Column<string>(nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quotation", x => x.quotationId);
                    table.ForeignKey(
                        name: "FK_Quotation_Location_destinationOfShipmentId",
                        column: x => x.destinationOfShipmentId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Quotation_Enquiry_enquiryId",
                        column: x => x.enquiryId,
                        principalTable: "Enquiry",
                        principalColumn: "enquiryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Quotation_Location_originOfShipmentId",
                        column: x => x.originOfShipmentId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Quotation_ServiceProvider_serviceProviderId",
                        column: x => x.serviceProviderId,
                        principalTable: "ServiceProvider",
                        principalColumn: "serviceProviderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Quotation_ShipmentTrackingLinks_shipmentTrackingId",
                        column: x => x.shipmentTrackingId,
                        principalTable: "ShipmentTrackingLinks",
                        principalColumn: "shipmentTrackingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Booking",
                columns: table => new
                {
                    bookingId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    additionalInformation = table.Column<string>(nullable: true),
                    bookingDate = table.Column<DateTime>(nullable: false),
                    bookingExpiryDate = table.Column<DateTime>(nullable: true),
                    bookingNumber = table.Column<string>(maxLength: 125, nullable: false),
                    bookingServiceFees = table.Column<decimal>(nullable: false),
                    bookingStatus = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(nullable: true),
                    destinationOfShipmentId = table.Column<int>(nullable: false),
                    eta = table.Column<DateTime>(nullable: false),
                    etd = table.Column<DateTime>(nullable: false),
                    expeditionType = table.Column<int>(nullable: false),
                    modeOfTransport = table.Column<int>(nullable: false),
                    originOfShipmentId = table.Column<int>(nullable: false),
                    quotationId = table.Column<string>(maxLength: 38, nullable: false),
                    serviceProviderId = table.Column<string>(nullable: true),
                    shipmentTrackingId = table.Column<int>(maxLength: 38, nullable: false),
                    terms = table.Column<string>(nullable: true),
                    trackingLink = table.Column<string>(nullable: true),
                    trackingReferenceNo = table.Column<string>(nullable: true),
                    transitDays = table.Column<string>(nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Booking", x => x.bookingId);
                    table.ForeignKey(
                        name: "FK_Booking_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Booking_Location_destinationOfShipmentId",
                        column: x => x.destinationOfShipmentId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Location_originOfShipmentId",
                        column: x => x.originOfShipmentId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Quotation_quotationId",
                        column: x => x.quotationId,
                        principalTable: "Quotation",
                        principalColumn: "quotationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Booking_ServiceProvider_serviceProviderId",
                        column: x => x.serviceProviderId,
                        principalTable: "ServiceProvider",
                        principalColumn: "serviceProviderId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Booking_ShipmentTrackingLinks_shipmentTrackingId",
                        column: x => x.shipmentTrackingId,
                        principalTable: "ShipmentTrackingLinks",
                        principalColumn: "shipmentTrackingId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "QuotationChargesLine",
                columns: table => new
                {
                    quotationChargesLineId = table.Column<string>(maxLength: 38, nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    chargeName = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    quotationId = table.Column<string>(maxLength: 38, nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationChargesLine", x => x.quotationChargesLineId);
                    table.ForeignKey(
                        name: "FK_QuotationChargesLine_Quotation_quotationId",
                        column: x => x.quotationId,
                        principalTable: "Quotation",
                        principalColumn: "quotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuotationContainerLine",
                columns: table => new
                {
                    quotationContainerLineId = table.Column<string>(maxLength: 38, nullable: false),
                    commodityType = table.Column<int>(nullable: false),
                    containerHeight = table.Column<decimal>(nullable: false),
                    containerLength = table.Column<decimal>(nullable: false),
                    containerType = table.Column<int>(nullable: false),
                    containerWidth = table.Column<decimal>(nullable: false),
                    count = table.Column<decimal>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    quotationId = table.Column<string>(maxLength: 38, nullable: true),
                    serviceType = table.Column<int>(nullable: false),
                    size = table.Column<decimal>(nullable: false),
                    userName = table.Column<string>(nullable: true),
                    volume = table.Column<decimal>(nullable: false),
                    weight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationContainerLine", x => x.quotationContainerLineId);
                    table.ForeignKey(
                        name: "FK_QuotationContainerLine_Quotation_quotationId",
                        column: x => x.quotationId,
                        principalTable: "Quotation",
                        principalColumn: "quotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingChargesLine",
                columns: table => new
                {
                    bookingChargesLineId = table.Column<string>(maxLength: 38, nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    bookingId = table.Column<string>(maxLength: 38, nullable: true),
                    chargeName = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingChargesLine", x => x.bookingChargesLineId);
                    table.ForeignKey(
                        name: "FK_BookingChargesLine_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingContainerLine",
                columns: table => new
                {
                    bookingContainerLineId = table.Column<string>(maxLength: 38, nullable: false),
                    bookingId = table.Column<string>(maxLength: 38, nullable: true),
                    commodityType = table.Column<int>(nullable: false),
                    containerHeight = table.Column<decimal>(nullable: false),
                    containerLength = table.Column<decimal>(nullable: false),
                    containerType = table.Column<int>(nullable: false),
                    containerWidth = table.Column<decimal>(nullable: false),
                    count = table.Column<decimal>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    serviceType = table.Column<int>(nullable: false),
                    size = table.Column<decimal>(nullable: false),
                    userName = table.Column<string>(nullable: true),
                    volume = table.Column<decimal>(nullable: false),
                    weight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingContainerLine", x => x.bookingContainerLineId);
                    table.ForeignKey(
                        name: "FK_BookingContainerLine_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingCustomerDocsLine",
                columns: table => new
                {
                    bookingCustomerDocsLineId = table.Column<string>(maxLength: 38, nullable: false),
                    bookingId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(maxLength: 38, nullable: true),
                    data = table.Column<byte[]>(nullable: true),
                    documentName = table.Column<string>(nullable: true),
                    documentType = table.Column<int>(nullable: false),
                    documentURL = table.Column<string>(nullable: true),
                    extension = table.Column<string>(nullable: true),
                    fileType = table.Column<string>(nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingCustomerDocsLine", x => x.bookingCustomerDocsLineId);
                    table.ForeignKey(
                        name: "FK_BookingCustomerDocsLine_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BookingCustomerDocsLine_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingServiceProviderDocsLine",
                columns: table => new
                {
                    bookingServiceProviderDocsLineId = table.Column<string>(maxLength: 38, nullable: false),
                    bookingId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    data = table.Column<byte[]>(nullable: true),
                    documentName = table.Column<string>(nullable: true),
                    documentType = table.Column<int>(nullable: false),
                    documentURL = table.Column<string>(nullable: true),
                    extension = table.Column<string>(nullable: true),
                    fileType = table.Column<string>(nullable: true),
                    serviceProviderId = table.Column<string>(maxLength: 38, nullable: true),
                    userName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingServiceProviderDocsLine", x => x.bookingServiceProviderDocsLineId);
                    table.ForeignKey(
                        name: "FK_BookingServiceProviderDocsLine_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BookingServiceProviderDocsLine_ServiceProvider_serviceProviderId",
                        column: x => x.serviceProviderId,
                        principalTable: "ServiceProvider",
                        principalColumn: "serviceProviderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_customerId",
                table: "Booking",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_destinationOfShipmentId",
                table: "Booking",
                column: "destinationOfShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_originOfShipmentId",
                table: "Booking",
                column: "originOfShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_quotationId",
                table: "Booking",
                column: "quotationId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_serviceProviderId",
                table: "Booking",
                column: "serviceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_shipmentTrackingId",
                table: "Booking",
                column: "shipmentTrackingId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingChargesLine_bookingId",
                table: "BookingChargesLine",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingContainerLine_bookingId",
                table: "BookingContainerLine",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingCustomerDocsLine_bookingId",
                table: "BookingCustomerDocsLine",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingCustomerDocsLine_customerId",
                table: "BookingCustomerDocsLine",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingServiceProviderDocsLine_bookingId",
                table: "BookingServiceProviderDocsLine",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingServiceProviderDocsLine_serviceProviderId",
                table: "BookingServiceProviderDocsLine",
                column: "serviceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_applicationUserId",
                table: "Customer",
                column: "applicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerLine_customerId",
                table: "CustomerLine",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_destinationOfShipmentId",
                table: "Enquiry",
                column: "destinationOfShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_originOfShipmentId",
                table: "Enquiry",
                column: "originOfShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EnquiryLine_enquiryId",
                table: "EnquiryLine",
                column: "enquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_fromUserId",
                table: "Notifications",
                column: "fromUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_toUserId",
                table: "Notifications",
                column: "toUserId");

            migrationBuilder.CreateIndex(
                name: "IX_PointOfContactsLine_serviceProviderId",
                table: "PointOfContactsLine",
                column: "serviceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_destinationOfShipmentId",
                table: "Quotation",
                column: "destinationOfShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_enquiryId",
                table: "Quotation",
                column: "enquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_originOfShipmentId",
                table: "Quotation",
                column: "originOfShipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_serviceProviderId",
                table: "Quotation",
                column: "serviceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_shipmentTrackingId",
                table: "Quotation",
                column: "shipmentTrackingId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationChargesLine_quotationId",
                table: "QuotationChargesLine",
                column: "quotationId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationContainerLine_quotationId",
                table: "QuotationContainerLine",
                column: "quotationId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProvider_applicationUserId",
                table: "ServiceProvider",
                column: "applicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderBanksLine_serviceProviderId",
                table: "ServiceProviderBanksLine",
                column: "serviceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicesLine_serviceProviderId",
                table: "ServicesLine",
                column: "serviceProviderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BookingChargesLine");

            migrationBuilder.DropTable(
                name: "BookingContainerLine");

            migrationBuilder.DropTable(
                name: "BookingCustomerDocsLine");

            migrationBuilder.DropTable(
                name: "BookingServiceProviderDocsLine");

            migrationBuilder.DropTable(
                name: "CustomerLine");

            migrationBuilder.DropTable(
                name: "EnquiryLine");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "PointOfContactsLine");

            migrationBuilder.DropTable(
                name: "QuotationChargesLine");

            migrationBuilder.DropTable(
                name: "QuotationContainerLine");

            migrationBuilder.DropTable(
                name: "ServiceProviderBanksLine");

            migrationBuilder.DropTable(
                name: "ServicesLine");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Booking");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Quotation");

            migrationBuilder.DropTable(
                name: "Enquiry");

            migrationBuilder.DropTable(
                name: "ServiceProvider");

            migrationBuilder.DropTable(
                name: "ShipmentTrackingLinks");

            migrationBuilder.DropTable(
                name: "Location");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
