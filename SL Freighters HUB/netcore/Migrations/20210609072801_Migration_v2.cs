﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsArticles",
                columns: table => new
                {
                    newsArticlesId = table.Column<string>(maxLength: 38, nullable: false),
                    authorId = table.Column<string>(nullable: true),
                    category = table.Column<string>(nullable: true),
                    content = table.Column<string>(nullable: false),
                    createdDate = table.Column<DateTime>(nullable: false),
                    featuredImage = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsArticles", x => x.newsArticlesId);
                    table.ForeignKey(
                        name: "FK_NewsArticles_AspNetUsers_authorId",
                        column: x => x.authorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NewsArticles_authorId",
                table: "NewsArticles",
                column: "authorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewsArticles");
        }
    }
}
