﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isAcceptedTerms",
                table: "ServiceProvider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isVerified",
                table: "ServiceProvider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isAcceptedTerms",
                table: "Customer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isVerified",
                table: "Customer",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isAcceptedTerms",
                table: "ServiceProvider");

            migrationBuilder.DropColumn(
                name: "isVerified",
                table: "ServiceProvider");

            migrationBuilder.DropColumn(
                name: "isAcceptedTerms",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "isVerified",
                table: "Customer");
        }
    }
}
