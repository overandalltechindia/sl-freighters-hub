using System;
using System.ComponentModel.DataAnnotations;

namespace netcore.Models
{
    public class NotificationMessageModel
    {
        public static string NewEnquiryMessage = "New Enquiry Received";

        public static string EnquiryAcceptedMessage = "Enquiry Accepted";

        public static string NewQuotationMessage = "New Quotation Received";

        public static string QuotationAcceptedMessage = "Quotation Accepted";

        public static string NewBookingMessage = "New Booking Received";

        public static string ShipmentCompleteMessage = "Shipment/Cargo Completed";

        public static string KYCMessage = "KYC";

        public static string DocumentRequestMessage;
    }


    public class NotificationNameModel
    {
        public static string EnquiryNotification = "Enquiry Notification";

        public static string QuotationNotification = "Quotation Notification";

        public static string BookingNotification = "Booking Notification";

        public static string CompletedNotification = "Shipment Completed";

        public static string DocumentRequestNotification = "Document Requested";

        public static string KYCApprovedNotification = "Your KYC Has Been Approved";

    }

    public class NotificationLinkModel
    {
        public static string EnquiriesLink = "Enquiry/Index";

        public static string QuotationsLink = "Quotation/Index";

        public static string BookingsLink = "Booking/Index";

        public static string ShipmentComplete = "Booking/Index";

        public static string DocumentRequest = "Booking/Index";

    }
}