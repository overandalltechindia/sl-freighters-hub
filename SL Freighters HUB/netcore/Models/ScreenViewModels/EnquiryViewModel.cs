﻿using netcore.Models.Invent;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ScreenViewModels
{
    public class EnquiryViewModel : INetcoreMasterChild
    {
        public EnquiryViewModel()
        {
            this.createdAt = DateTime.Now.Date;
            //this.enquiryNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#EN";
            this.enquiryNumber = "EN" + Guid.NewGuid().ToString().Substring(0, 5).ToUpper();
            this.enquiryDate = DateTime.Now.Date;
            this.enquiryExpiryDate = this.enquiryDate.AddDays(5);

            //enquiryViewModelLine = new List<EnquiryLine>();
        }

        //[Display(Name = "Enquiry Line")]
        ////public List<EnquiryLineViewModel> enquiryViewModelLine { get; set; }
        //public List<EnquiryLine> enquiryViewModelLine { get; set; }


        [StringLength(38)]
        [Display(Name = "Enquiry Id")]
        [Key]
        public string enquiryId { get; set; }

        [StringLength(125)]
        [Required]
        [Display(Name = "Enquiry Number")]
        public string enquiryNumber { get; set; }

        [Display(Name = "Enquiry Date")]
        public DateTime enquiryDate { get; set; }

        [Display(Name = "Enquiry Expiry Date")]
        public DateTime? enquiryExpiryDate { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Mode Of Transport")]
        public ModeOfTransport modeOfTransport { get; set; }





        //FCL

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Origin Type")]
        public LocationType fclOriginLocationType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Destination Type")]
        public LocationType fclDestinationLocationType { get; set; }

        [Required]
        [Display(Name = "Origin of Shipment FCL")]
        public int fclOriginOfShipmentId { get; set; }

        [Required]
        [Display(Name = "Destination of Shipment FCL")]
        public int fclDestinationOfShipmentId { get; set; }


        public string fclPickupAddressPincode { get; set; }
        public string fclDeliveryAddressPincode { get; set; }


        //LCL

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Origin Type")]
        public LocationType lclOriginLocationType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Destination Type")]
        public LocationType lclDestinationLocationType { get; set; }

        [Display(Name = "Origin of Shipment")]
        public int lclOriginOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        public int lclDestinationOfShipmentId { get; set; }

        public string lclPickupAddressPincode { get; set; }
        public string lclDeliveryAddressPincode { get; set; }


        //AIR

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Origin Type")]
        public LocationType airOriginLocationType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Destination Type")]
        public LocationType airDestinationLocationType { get; set; }

        [Display(Name = "Origin of Shipment")]
        public int airOriginOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        public int airDestinationOfShipmentId { get; set; }

        public string airPickupAddressPincode { get; set; }
        public string airDeliveryAddressPincode { get; set; }




        //FTL

        [Display(Name = "Origin of Shipment")]
        public int ftlOriginOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        public int ftlDestinationOfShipmentId { get; set; }

        public string ftlPickupAddressPincode { get; set; }
        public string ftlDeliveryAddressPincode { get; set; }


        //LTL
        [Display(Name = "Origin of Shipment")]
        public int ltlOriginOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        public int ltlDestinationOfShipmentId { get; set; }
        public string ltlPickupAddressPincode { get; set; }
        public string ltlDeliveryAddressPincode { get; set; }



        //Rail
        public string railPickupAddressPincode { get; set; }
        public string railDeliveryAddressPincode { get; set; }




        public bool hasCargoPickup { get; set; }

        public bool hasCargoDelivery { get; set; }

        public bool hasPortForwardingFees { get; set; }

        public bool hasCustomClearance { get; set; }

        public bool hasLogisticsTradeFinance { get; set; }

        public bool hasCargoInsurance { get; set; }
        public string insuranceValue { get; set; }


        [Display(Name = "Comments")]
        public string comments { get; set; }

        [Display(Name = "Enquiry Status")]
        public EnquiryStatus enquiryStatus { get; set; }

        

    }
}
