﻿using netcore.Models.Invent;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ScreenViewModels
{
    public class AnalyticsViewModel
    {

        [StringLength(38)]
        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [StringLength(125)]
        [Required]
        [Display(Name = "Booking Number")]
        public string bookingNumber { get; set; }

        [Display(Name = "Quotation Date")]
        public DateTime bookingDate { get; set; }

        [Display(Name = "Shipment Name")]
        public string shipmentName { get; set; }

        [Display(Name = "ETD")]
        public DateTime etd { get; set; }

        [Display(Name = "ETA")]
        public DateTime eta { get; set; }

        [Display(Name = "Transit Days")]
        public string transitDays { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Expedition Type")]
        public ExpeditionType expeditionType { get; set; }


        [Display(Name = "Booking Expiry Date")]
        public DateTime? bookingExpiryDate { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Enquiry Number")]
        public string enquiryId { get; set; }

        [Display(Name = "Enquiry")]
        public Enquiry enquiry { get; set; }

        [Display(Name = "Terms")]
        public string terms { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Mode Of Transport")]
        public ModeOfTransport modeOfTransport { get; set; }

        [StringLength(38)]
        [Display(Name = "Origin of Shipment Id")]
        public string originOfShipmentId { get; set; }

        public string originOfShipmentLocationName { get; set; }

        public string destinationOfShipmentLocationName { get; set; }

        [StringLength(38)]
        [Display(Name = "Destination of Shipment Id")]
        public string destinationOfShipmentId { get; set; }

        [Display(Name = "Additional Information")]
        public string additionalInformation { get; set; }


        [Display(Name = "Booking Service Fees")]
        public decimal bookingServiceFees { get; set; }


        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Booking Status")]
        public BookingStatus bookingStatus { get; set; }


        [Display(Name = "Enquiry List")]
        public List<Enquiry> enquiryList { get; set; } = new List<Enquiry>();

        [Display(Name = "Booking List")]
        public List<Booking> bookingList { get; set; } = new List<Booking>();

        [Display(Name = "Quotation Line")]
        public List<Quotation> quotationList { get; set; } = new List<Quotation>();

        [Display(Name = "Service Providers Received Enquiry")]
        public List<ServiceProvider> serviceProvidersReceivedEnquiryList { get; set; } = new List<ServiceProvider>();

        [Display(Name = "Service Providers Sbumitted Quotation")]
        public List<ServiceProvider> serviceProvidersSubmittedQuoteList { get; set; } = new List<ServiceProvider>();
    }
}
