﻿using netcore.Models.Invent;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ScreenViewModels
{
    public class ServiceProviderViewModel
    {
        [StringLength(38)]
        [Display(Name = "Service Provider Id")]
        public string serviceProviderId { get; set; }

        [Display(Name = "Application User Id")]
        public string applicationUserId { get; set; }

        [Display(Name = "Application User")]
        public ApplicationUser applicationUser { get; set; }

        [StringLength(126)]
        [Display(Name = "Company Name")]
        [Required]
        public string companyName { get; set; }

        [StringLength(50)]
        [Display(Name = "Business Contact Name")]
        [Required]
        public string businessContactName { get; set; }

        [StringLength(126)]
        [Display(Name = "Description")]
        public string description { get; set; }

        //[Display(Name = "Business Size")]
        //public BusinessSize size { get; set; }

        [Display(Name = "Official Address")]
        [Required]
        [StringLength(256)]
        public string officialAddress { get; set; }

        [Display(Name = "Primary Email")]
        [StringLength(30)]
        public string primaryEmail { get; set; }

        [Display(Name = "Primary Contact")]
        [StringLength(12)]
        public string primaryContact { get; set; }

        [Display(Name = "Location")]
        [Required]
        [StringLength(256)]
        public string location { get; set; }

        [Display(Name = "Service Provided")]
        public List<ServicesLine> servicesLine { get; set; } = new List<ServicesLine>();

        [Display(Name = "Point of Contacts")]
        public List<PointOfContactsLine> pointOfContactsLine { get; set; } = new List<PointOfContactsLine>();





        //Point Of Contacts & Banks

        [StringLength(38)]
        [Display(Name = "Point Of Contact Id")]
        public string pointOfContactLineId { get; set; }

        [Display(Name = "ServiceProvider")]
        public ServiceProvider serviceProvider { get; set; }

        [StringLength(126)]
        [Display(Name = "Name")]
        [Required]
        public string name { get; set; }

        [Display(Name = "Designation")]
        [Required]
        [StringLength(50)]
        public string designation { get; set; }

        [Display(Name = "Contact")]
        [Required]
        [StringLength(50)]
        public string contact { get; set; }

        [Display(Name = "Email")]
        [Required]
        [StringLength(50)]
        public string email { get; set; }

        [Display(Name = "Current Point Of Contact ?")]
        public bool isDefaultPOC { get; set; } = false;


        //Bank Details 

        [StringLength(126)]
        [Display(Name = "Bank Name")]
        [Required]
        public string bankName { get; set; }

        [StringLength(126)]
        [Display(Name = "Account Number")]
        [Required]
        public string accountNumber { get; set; }

        [StringLength(126)]
        [Display(Name = "Account Holder Name")]
        [Required]
        public string accountHolderName { get; set; }

        [StringLength(126)]
        [Display(Name = "IFSC Code")]
        [Required]
        public string ifscCode { get; set; }





        //My Services

        [StringLength(38)]
        [Display(Name = "Services Line Id")]
        public string servicesLineId { get; set; }

        [Display(Name = "Service Type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ServicesType serviceType { get; set; }





        //KYC
        [Display(Name = "Country Of Registration")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Countries countryOfRegistration { get; set; }

        [Display(Name = "GST No.")]
        [StringLength(15)]
        public string gstNo { get; set; }

        [Display(Name = "PAN No.")]
        [StringLength(10)]
        public string panNo { get; set; }

        [Display(Name = "Address Proof Document")]
        public string addressProofDocument { get; set; }
    }
}
