﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ScreenViewModels
{
    public class BookingChargesLineViewModel
    {

        [StringLength(38)]
        [Display(Name = "Booking Charges Line Id")]
        public string bookingChargesLineLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Booking Id")]
        public string boo9kingId { get; set; }

        [Display(Name = "Booking")]
        public BookingViewModel quotation { get; set; }

        [Display(Name = "Charge Name")]
        public string chargeName { get; set; }

        [Display(Name = "Amount")]  
        public decimal amount { get; set; }
    }
}
