﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ScreenViewModels
{
    public class QuotationChargesLineViewModel
    {

        [StringLength(38)]
        [Display(Name = "Quotation Charges Line Id")]
        public string quotationChargesLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Quotation Id")]
        public string quotationId { get; set; }

        [Display(Name = "Quotation")]
        public QuotationViewModel quotation { get; set; }

        [Display(Name = "Charge Name")]
        public string chargeName { get; set; }

        [Display(Name = "Amount")]  
        public decimal amount { get; set; }
    }
}
