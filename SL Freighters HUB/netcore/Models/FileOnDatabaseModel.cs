﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models
{
    public class FileOnDatabaseModel
    {
        public string name { get; set; }
        public string fileType { get; set; }
        public string extension { get; set; }
        public byte[] data { get; set; }
    }
}
