﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models
{
    public class NewsArticles
    {
        [StringLength(38)]
        [Display(Name = "News Articles Id")]
        [Key]
        public string newsArticlesId { get; set; }

        [Display(Name = "Title of the Article")]
        [Required]
        public string title { get; set; }

        [Display(Name = "Category")]
        public string category { get; set; }

        [Display(Name = "author Id")]
        public string authorId { get; set; }

        [Display(Name = "Author Username")]
        public ApplicationUser author { get; set; }

        [Display(Name = "Featured Image")]
        public string featuredImage { get; set; }

        [Display(Name = "Compose New Article")]
        [Required]
        public string content { get; set; }

        [Display(Name = "Posted Date")]
        public DateTime? postedDate { get; set; }

        [Display(Name = "Create Date")]
        public DateTime createdDate { get; set; }

        [Display(Name = "Post this article?")]
        public bool isPosted { get; set; }
    }
}
