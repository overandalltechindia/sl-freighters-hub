﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Booking")]
    public class Booking : INetcoreMasterChild
    {
        public Booking()
        {
            this.createdAt = DateTime.Now + DateTime.Now.TimeOfDay;
            this.bookingNumber = "BK" + Guid.NewGuid().ToString().Substring(0, 5).ToUpper();
            this.bookingDate = DateTime.Now + DateTime.Now.TimeOfDay;
            this.bookingExpiryDate = this.bookingDate.AddDays(10);
            this.bookingStatus = BookingStatus.Ongoing;

            this.etd = DateTime.Now.Date;
            this.eta = DateTime.Now.Date.AddDays(10);
        }

        [StringLength(38)]
        [Display(Name = "Booking Id")]
        [Key]
        public string bookingId { get; set; }

        [StringLength(125)]
        [Required]
        [Display(Name = "Booking Number")]
        public string bookingNumber { get; set; }

        [Display(Name = "Booking Date")]
        public DateTime bookingDate { get; set; }

        //[Display(Name = "Shipment Name")]
        //public string shipmentName { get; set; }

        [Required]
        [Display(Name = "Shipment Name")]
        public int shipmentTrackingId { get; set; }

        [Display(Name = "Shipment Name")]
        public ShipmentTrackingLinks shipmentTrackingLinks { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }

        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Service Provider")]
        public ServiceProvider serviceProvider { get; set; }

        [Display(Name = "ServiceProvider Id")]
        public string serviceProviderId { get; set; }

        [Display(Name = "ETD")]
        public DateTime etd { get; set; }

        [Display(Name = "ETA")]
        public DateTime eta { get; set; }

        [Display(Name = "Transit Days")]
        public string transitDays { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Expedition Type")]
        public ExpeditionType expeditionType { get; set; }


        [Display(Name = "Booking Expiry Date")]
        public DateTime? bookingExpiryDate { get; set; }

        //[StringLength(38)]
        //[Required]
        //[Display(Name = "Enquiry Number")]
        //public string enquiryId { get; set; }

        //[Display(Name = "Enquiry")]
        //public Enquiry enquiry { get; set; }


        [StringLength(38)]
        [Required]
        [Display(Name = "Quotation Number")]
        public string quotationId { get; set; }

        [Display(Name = "Quotation")]
        public Quotation quotation { get; set; }

        [Display(Name = "Terms")]
        public string terms { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Mode Of Transport")]
        public ModeOfTransport modeOfTransport { get; set; }

        [Display(Name = "Origin of Shipment Id")]
        public int originOfShipmentId { get; set; }

        [NotMapped]
        public string originOfShipmentLocationName { get; set; }

        [NotMapped]
        public string destinationOfShipmentLocationName { get; set; }

        [Display(Name = "Origin of Shipment")]
        [ForeignKey("originOfShipmentId")]
        public Location originOfShipment { get; set; }

        [Display(Name = "Destination of Shipment Id")]
        public int destinationOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        [ForeignKey("destinationOfShipmentId")]
        public Location destinationOfShipment { get; set; }

        [Display(Name = "Additional Information")]
        public string additionalInformation { get; set; }

        [Display(Name = "Tracking Link")]
        public string trackingLink { get; set; }

        [Display(Name = "Tracking Reference Number")]
        public string trackingReferenceNo { get; set; }


        [Display(Name = "Booking Service Fees")]
        public decimal bookingServiceFees { get; set; }


        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Booking Status")]
        public BookingStatus bookingStatus { get; set; }

        [Display(Name = "Booking Charges Line")]
        public List<BookingChargesLine> bookingChargesLine { get; set; } = new List<BookingChargesLine>();

        [Display(Name = "Booking Container Line")]
        public List<BookingContainerLine> bookingContainerLine { get; set; } = new List<BookingContainerLine>();

        [Display(Name = "Booking Service Provider Docs Line")]
        public List<BookingServiceProviderDocsLine> bookingServiceProviderDocsLine { get; set; } = new List<BookingServiceProviderDocsLine>();
    }
}
