﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Customer : INetcoreMasterChild
    {
        public Customer()
        {
            this.createdAt = DateTime.Now.Date;
            this.registrationDate = DateTime.Now.Date;
            this.registrationExpDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Application User Id")]
        public string applicationUserId { get; set; }

        [Display(Name = "Application User")]
        public ApplicationUser applicationUser { get; set; }

        [StringLength(50)]
        [Display(Name = "Customer Name")]
        public string customerName { get; set; }


        [Display(Name = "Registration Date")]
        public DateTime? registrationDate { get; set; }


        [Display(Name = "Expiry Date")]
        public DateTime? registrationExpDate { get; set; }


        [StringLength(126)]
        [Display(Name = "Company Name")]
        public string companyName { get; set; }

        [StringLength(50)]
        [Display(Name = "Business Contact Name")]
        public string businessContactName { get; set; }

        [StringLength(126)]
        [Display(Name = "Description")]
        public string description { get; set; }

        [Display(Name = "Address")]
        [StringLength(256)]
        public string officialAddress { get; set; }

        [Display(Name = "Primary Email")]
        [StringLength(30)]
        public string primaryEmail { get; set; }

        [Display(Name = "Primary Contact")]
        [StringLength(12)]
        public string primaryContact { get; set; }

        [Display(Name = "PIN Code")]
        [StringLength(10)]
        public string street2 { get; set; } // use as Pin code

        [Display(Name = "Location")]
        [StringLength(256)]
        public string location { get; set; }

        [Display(Name = "GST No.")]
        [StringLength(15)]
        public string gstNo { get; set; }

        [Display(Name = "PAN No.")]
        [StringLength(10)]
        public string panNo { get; set; }

        public bool isAcceptedTerms { get; set; }
        public bool isVerified { get; set; }

        //IBaseAddress

        [Display(Name = "Customer Contacts")]
        public List<CustomerLine> customerLine { get; set; } = new List<CustomerLine>();
    }
}
