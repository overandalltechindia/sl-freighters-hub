﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class BookingCustomerDocsLine : INetcoreBasic
    {
        public BookingCustomerDocsLine()
        {
            this.createdAt = DateTime.Now;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Booking Service Provider Docs Line Id")]
        public string bookingCustomerDocsLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [StringLength(38)]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Service Provider")]
        public Customer customer { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Document Type")]
        public DocumentType documentType { get; set; }

        [Display(Name = "Document URL")]
        public string documentURL { get; set; }

        public string documentName { get; set; }
        public string fileType { get; set; }
        public string extension { get; set; }
        public byte[] data { get; set; }
    }
}
