﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum PackageType
    {
        [Display(Name = "PALLET")]
        PALLET = 1,

        [Display(Name = "CRATE")]
        CRATE = 2,

        [Display(Name = "BOX")]
        BOX = 3,

        [Display(Name = "BALE")]
        BALE = 4,

        [Display(Name = "BAG")]
        BAG = 5,

        [Display(Name = "BUNDLE")]
        BUNDLE = 6,

        [Display(Name = "CAN")]
        CAN = 7,


    }
}
