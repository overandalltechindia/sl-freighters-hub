﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum LoadClosedBodyType
    {

        [Display(Name = "LCV (14ft) (3.5 ton)")]
        LCV_14FT_3_5Ton = 1,

        [Display(Name = "LCV (17 ft) (5 ton)")]
        LCV_17FT_5Ton = 2,

        [Display(Name = "LCV (19ft) (7 ton)")]
        LCV_19FT_7Ton = 3,

        [Display(Name = "32 ft single axle (7 ton)")]
        FT32_SA_7Ton = 4,

        [Display(Name = "32 ft single axle HQ (7 ton)")]
        FT32_SAHQ_7Ton = 5,

        [Display(Name = "19-22 ft single axle (7 ton)")]
        FT_1922_SA_7Ton = 6,

        [Display(Name = "24 ft single axle (7 ton)")]
        FT_24_SA_7Ton = 7,

        [Display(Name = "32 ft multi axle HQ (14-15 ton)")]
        FT_32_SAHQ_14_15Ton = 8,

        [Display(Name = "32 ft multi axle (14-15 ton)")]
        FT32_MA_1415Ton = 9,

        [Display(Name = "32ft triple axle (20-21 ton)")]
        FT32_TA_2021Ton = 10,

        [Display(Name = "32 ft triple axle HQ (20-21 ton)")]
        FT32_TAHQ_2021Ton = 11,

    }
}
