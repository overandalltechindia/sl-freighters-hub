﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class QuotationChargesLine : INetcoreBasic
    {
        public QuotationChargesLine()
        {
            this.createdAt = DateTime.Now.Date;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Quotation Charges Line Id")]
        public string quotationChargesLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Quotation Id")]
        public string quotationId { get; set; }

        [Display(Name = "Quotation")]
        public Quotation quotation { get; set; }

        [Display(Name = "Charge Name")]
        public string chargeName { get; set; }

        [Display(Name = "Amount in (INR)")]  
        public decimal amount { get; set; }
    }
}
