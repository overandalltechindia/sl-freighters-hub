﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum QuotationStatus
    {
        Draft = -1,
        Submitted = 1,
        Approved = 2,
        Expired = 3
    }
}
