﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Location")]
    public class Location
    {
        public Location()
        {
           
        }

        [Key]
        [Display(Name = "Location Id")]
        public int locationId { get; set; }

        [StringLength(256)]
        [Display(Name = "Location Name")]
        [Required]
        public string locationName { get; set; }

        [Display(Name = "Country")]
        public string countryProvince { get; set; }

        [Display(Name = "Code")]
        public string code { get; set; }

        [Display(Name = "Location Type")]
        public string locationType { get; set; }
    }
}
