﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ServicesType
    {
        FCL = 1,

        LCL = 2,

        Air = 3,

        FTL = 4,

        LTL = 5,

        RAIL = 6,

        Haulage = 7

        //Trailer = 8,

        //Insurance = 9,

        //Customs = 10,

        //Credit = 11,

        //Others = 12
    }
}
