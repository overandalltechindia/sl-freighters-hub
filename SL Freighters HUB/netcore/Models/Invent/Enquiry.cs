﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Enquiry")]
    public class Enquiry : INetcoreMasterChild
    {
        public Enquiry()
        {
            this.createdAt = DateTime.Now + DateTime.Now.TimeOfDay;
            //this.enquiryNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#EN";
            this.enquiryNumber = "EN" + Guid.NewGuid().ToString().Substring(0, 5).ToUpper();
            this.enquiryDate = DateTime.Now + DateTime.Now.TimeOfDay;
            this.enquiryExpiryDate = this.enquiryDate.AddDays(5);
        }

        [StringLength(38)]
        [Display(Name = "Enquiry Id")]
        [Key]
        public string enquiryId { get; set; }

        [StringLength(125)]
        [Required]
        [Display(Name = "Enquiry Number")]
        public string enquiryNumber { get; set; }

        [Display(Name = "Enquiry Date")]
        public DateTime enquiryDate { get; set; }

        [Display(Name = "Enquiry Expiry Date")]
        public DateTime? enquiryExpiryDate { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Mode Of Transport")]
        public ModeOfTransport modeOfTransport { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Origin Type")]
        public LocationType originLocationType { get; set; }

        [Display(Name = "Origin of Shipment")]
        public int? originOfShipmentId { get; set; }

        [Display(Name = "Origin of Shipment")]
        [ForeignKey("originOfShipmentId")]
        public Location originOfShipment { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Destination Type")]
        public LocationType destinationLocationType { get; set; }

        [Display(Name = "Destination of Shipment")]
        public int? destinationOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        [ForeignKey("destinationOfShipmentId")]
        public Location destinationOfShipment { get; set; }

        public bool hasCargoPickup { get; set; }

        [Display(Name = "Pickup Address & Pincode")]
        public string pickupAddressPincode { get; set; }

        public bool hasCargoDelivery { get; set; }

        [Display(Name = "Delivery Address & Pincode")]

        public string deliveryAddressPincode { get; set; }

        public bool hasPortForwardingFees { get; set; }

        public bool hasCustomClearance { get; set; }

        public bool hasLogisticsTradeFinance { get; set; }

        public bool hasCargoInsurance { get; set; }
        public string insuranceValue { get; set; }


        [Display(Name = "Comments")]
        public string comments { get; set; }

        [Display(Name = "Enquiry Status")]
        public EnquiryStatus enquiryStatus { get; set; }

        [Display(Name = "Enquiry Line")]
        public List<EnquiryLine> enquiryLine { get; set; } = new List<EnquiryLine>();

    }
}
