﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class BookingContainerLine : INetcoreBasic
    {
        public BookingContainerLine()
        {
            this.createdAt = DateTime.Now.Date;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Booking Container Line Id")]
        public string bookingContainerLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Service Type")]
        public ServicesType serviceType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Container Type")]
        public ContainerType containerType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Commodity Type")]
        public CommodityType commodityType { get; set; }

        [Display(Name = "Volume")]  //For LCL
        public decimal volume { get; set; }     //In CBM

        [Display(Name = "Weight")]  //For LCL
        public decimal weight { get; set; }     //In Kgs/ Metric Tons

        [Display(Name = "Size")]  //For LCL
        public decimal size { get; set; }     //In Ft

        [Display(Name = "Length")]  //For LCL
        public decimal containerLength { get; set; }

        [Display(Name = "Width")]  //For LCL
        public decimal containerWidth { get; set; }

        [Display(Name = "Height")]  //For LCL
        public decimal containerHeight { get; set; }

        [Display(Name = "Quantity")]  //For LCL
        public decimal count { get; set; }
    }
}
