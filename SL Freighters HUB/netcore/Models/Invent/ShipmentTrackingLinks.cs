﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ShipmentTrackingLinks
    {
        [Key]
        [Display(Name = "Shipment Line Id")]
        public int shipmentTrackingId { get; set; }

        [Display(Name = "Shipment Name")]
        public string shipmentName { get; set; }

        [Display(Name = "Tracking Link")]
        public string shipmentTrackingLink { get; set; }

    }
}
