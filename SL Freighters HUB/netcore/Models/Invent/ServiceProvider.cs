﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ServiceProvider : INetcoreMasterChild
    {
        public ServiceProvider()
        {
            this.createdAt = DateTime.Now.Date;
            this.isAcceptedTerms = false;
            this.isVerified = false;

        }

        [StringLength(38)]
        [Display(Name = "Service Provider Id")]
        public string serviceProviderId { get; set; }

        [Display(Name = "Application User Id")]
        public string applicationUserId { get; set; }

        [Display(Name = "Application User")]
        public ApplicationUser applicationUser { get; set; }

        [StringLength(126)]
        [Display(Name = "Company Name")]
        public string companyName { get; set; }

        [StringLength(50)]
        [Display(Name = "Business Contact Name")]
        public string businessContactName { get; set; }

        [StringLength(126)]
        [Display(Name = "Description")]
        public string description { get; set; }
        
        //[Display(Name = "Business Size")]
        //public BusinessSize size { get; set; }

        [Display(Name = "Official Address")]
        [StringLength(256)]
        public string officialAddress { get; set; }

        [Display(Name = "Primary Email")]
        [StringLength(30)]
        public string primaryEmail { get; set; }

        [Display(Name = "Primary Contact")]
        [StringLength(12)]
        public string primaryContact { get; set; }


        [Display(Name = "Country Of Registration")]
        //[StringLength(156)]
        //public string countryOfRegistration { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Countries countryOfRegistration { get; set; } = Countries.IN;

        [Display(Name = "Location")]
        [StringLength(256)]
        public string location { get; set; }

        [Display(Name = "GST No.")]
        [StringLength(15)]
        public string gstNo { get; set; }

        [Display(Name = "PAN No.")]
        [StringLength(10)]
        public string panNo { get; set; }

        [Display(Name = "Address Proof Document")]
        public string addressProofDocument { get; set; }

        [Display(Name = "isAcceptedTerms")]
        public bool isAcceptedTerms { get; set; } = true;

        [Display(Name = "isVerified")]
        public bool isVerified { get; set; } = false;


        [Display(Name = "Service Provided")]
        public List<ServicesLine> servicesLine { get; set; } = new List<ServicesLine>();

        [Display(Name = "Point of Contacts")]
        public List<PointOfContactsLine> pointOfContactsLine { get; set; } = new List<PointOfContactsLine>();
    }
}
