﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ProductCategory
    {
        [Display(Name = "APOLLO")]
        APOLLO = 1,
        [Display(Name = "ATLAS")]
        ATLAS = 2,
        [Display(Name = "ANDER")]
        ANDER = 3,
        [Display(Name = "AJAX")]
        AJAX = 4,
        [Display(Name = "AURA")]
        AURA  = 5,
        [Display(Name = "ATHEN")]
        ATHEN = 6,
        [Display(Name = "ASTRA")]
        ASTRA = 7,
        [Display(Name = "ALETA")]
        ALETA = 8,
        [Display(Name = "AETOS")]
        AETOS = 9,

        [Display(Name = "AETOS (Round Dumbbells)")]
        AETOSRound = 10,
        [Display(Name = "AETOS (Hex Dumbbells)")]
        AETOSHex = 11,
        [Display(Name = "AETOS (Double color plates)")]
        AETOSDouble = 12,
        [Display(Name = "AETOS (Tri Grip plates)")]
        AETOSTri = 13,
        [Display(Name = "AETOS (Rubber Bumper Plates)")]
        AETOSRubber = 14,


        [Display(Name = "GYM FLOORING")]
        GYMF = 15,

        [Display(Name = "OFFICE MISC.")]
        OFFMISC = 16,

        [Display(Name = "WAREHOUSE MISC.")]
        WHMISC = 17,

        [Display(Name = "SPARE PARTS MISC.")]
        SPMISC = 18,

        [Display(Name = "OTHER MISC.")]
        OTHMISC = 19,

    }
}
