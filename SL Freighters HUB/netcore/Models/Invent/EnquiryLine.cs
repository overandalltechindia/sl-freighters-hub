﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class EnquiryLine : INetcoreBasic
    {
        public EnquiryLine()
        {
            this.createdAt = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Enquiry Line Id")]
        public string enquiryLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Enquiry Id")]
        [Required]
        public string enquiryId { get; set; }

        [Display(Name = "Enquiry")]
        public Enquiry enquiry { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Service Type")]
        public ServicesType serviceType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Container Type")]
        public ContainerType containerType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Load Type")]
        public LoadType loadType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Closed Body Load Type")]
        public LoadClosedBodyType closedLoadType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Package Type")]
        public PackageType packageType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Commodity Type")]
        public CommodityType commodityType { get; set; }

        [Display(Name = "Volume")]  //For LCL
        public decimal? volume { get; set; }     //In CBM

        [Display(Name = "Gross Weight")]  //For LCL
        public decimal? weight { get; set; }     //In Kgs/ Metric Tons

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Weight Unit")]
        public UOM weightUOM { get; set; }

        [Display(Name = "Refrigerated")]  
        public bool isRefrigerated { get; set; }

        [Display(Name = "isClosedBodyTruck")]
        public bool isClosedBodyTruck { get; set; }

        [Display(Name = "Temperature")]  //In Degrees C
        public decimal? temperature { get; set; }     

        [Display(Name = "Length")]  //For LCL
        public decimal? containerLength { get; set; }

        [Display(Name = "Width")]  //For LCL
        public decimal? containerWidth { get; set; }

        [Display(Name = "Height")]  //For LCL
        public decimal? containerHeight { get; set; }

        [Display(Name = "Quantity")]  //For ALL SERVICES
        public decimal? count { get; set; }

    }
}
