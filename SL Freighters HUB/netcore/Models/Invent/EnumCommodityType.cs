﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum CommodityType
    {   
        [Display(Name = "General")]
        General  = 1,
        [Display(Name = "Hazardous")]
        Hazardous = 2,
        [Display(Name = "Miscellaneous")]
        Misc = 3,
        [Display(Name = "Gases")]
        Gases = 4,
        [Display(Name = "Flammable")]
        Flammable = 5,
        [Display(Name = "Corrosives")]
        Corrosives = 6,
        [Display(Name = "Radioactive Materials")]
        Radioactive = 7,
        [Display(Name = "Live animals")]
        Animals = 8,
        [Display(Name = "Pharma.")]
        Pharma = 9,
        [Display(Name = "Perishable")]
        Perishable = 10,
        [Display(Name = "Consumer Equipments")]
        Equipments = 11,
    }
}
