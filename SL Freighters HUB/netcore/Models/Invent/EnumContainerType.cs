﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ContainerType
    {
        [Display(Name = "20 FT STANDARD")]
        FT20_STD = 1,

        [Display(Name = "40 FT STANDARD")]
        FT40_STD = 2,

        [Display(Name = "40 FT HIGH CUBE")]
        FT40_HC = 3,

        [Display(Name = "45 FT HIGH CUBE")]
        FT45_HC = 4,

        [Display(Name = "20 FT OPEN TOP")]
        FT20_OPENTOP = 5,

        [Display(Name = "40 FT OPEN TOP")]
        FT40_OPENTOP = 6,

        [Display(Name = "20 FLATRACK")]
        FT20_FLATRACK = 7,

        [Display(Name = "40 FT FLATRACK")]
        FT40_FLATRACK = 8,


    }
}
