﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum LoadType
    {   

        [Display(Name = "Pickup (1 ton)")]
        Pickup_1_Ton = 1,

        [Display(Name = "Tata Ace (750-800 kgs)")]
        Tata_Ace_750_800Kg = 2,

        [Display(Name = "Tata 407 (2.5 ton)")]
        Tata_407_2_5_Ton = 3,

        [Display(Name = "LCV (14ft) (4 ton)")]
        LCV_14FT_4Ton = 4,

        [Display(Name = "LCV (17 ft) (5 ton)")]
        LCV_17FT_5Ton = 5,

        [Display(Name = "LCV (19ft) (7 ton)")]
        LCV_19FT_7Ton = 6,

        [Display(Name = "6 tyre (19-24 ft) (9 ton)")]
        Six_Tyre_19_24FT_9Ton = 7,

        [Display(Name = "10 tyre Taurus 22ft (16 ton)")]
        Ten_Tyre_22_FT_16Ton = 8,

        [Display(Name = "12 tyre Taurus 22 ft (22 ton)")]
        Twelve_Tyre_22_FT_22Ton = 9,

        [Display(Name = "14 tyre Taurus 22ft (26 ton)")]
        Forteen_Tyre_22_FT_26Ton = 10,

        [Display(Name = "18 tyre (28 ton)")]
        Eighteen_Tyre_28Ton = 11,

        [Display(Name = "22 tyre (35 ton)")]
        Twentytwo_Tyre_35Ton = 12,

        [Display(Name = "40ft Flat Bed Trailor (20-25 ton)")]
        FortyFT_FB_20_25Ton = 13,

        [Display(Name = "40ft Semi Low Bed Trailor (20-25 ton)")]
        FortyFT_SB_20_25Ton = 14,

        [Display(Name = "40ft Low Bed Trailor (20-25-30 ton)")]
        FortyFT_LB_20_25_30Ton = 15,

    }
}
