﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum BookingStatus
    {
        New = 1,
        Ongoing = 2,
        Closed = 3
    }
}
