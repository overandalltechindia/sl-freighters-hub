﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class PointOfContactsLine : INetcoreBasic
    {
        public PointOfContactsLine()
        {
            this.createdAt = DateTime.Now.Date;
            this.isDefaultPOC = false;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Point Of Contact Id")]
        public string pointOfContactLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Service Provider Id")]
        public string serviceProviderId { get; set; }
        
        [Display(Name = "ServiceProvider")]
        public ServiceProvider serviceProvider { get; set; }

        [StringLength(126)]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Designation")]
        [Required]
        [StringLength(50)]
        public string designation { get; set; }

        [Display(Name = "Contact")]
        [StringLength(50)]
        public string contact { get; set; }

        [Display(Name = "Email")]
        [StringLength(50)]
        public string email { get; set; }

        [Display(Name = "Current Point Of Contact ?")]
        public bool isDefaultPOC { get; set; }

    }
}
