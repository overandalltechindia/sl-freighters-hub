﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ModeOfTransport
    {
        Road = 1,
        Rail = 2,
        Ocean = 3,
        Air = 4
    }
}
