﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ServicesLine : INetcoreBasic
    {
        public ServicesLine()
        {
            this.createdAt = DateTime.Now.Date;
        }

        [Key]
        [StringLength(38)]
        [Display(Name = "Services Line Id")]
        public string servicesLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Service Provider Id")]
        public string serviceProviderId { get; set; }

        [Display(Name = "ServiceProvider")]
        public ServiceProvider serviceProvider { get; set; }

        [Display(Name = "Service Type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ServicesType serviceType { get; set; }
    }
}
