﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum EnquiryStatus
    {
        Pending = 1,
        Accepted = 2,
        Closed = 3
    }
}
