﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ExpeditionType
    {
        Direct = 1,
        Transfer = 2
    }
}
