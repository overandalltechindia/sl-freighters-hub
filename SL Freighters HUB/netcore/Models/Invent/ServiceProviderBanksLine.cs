﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ServiceProviderBanksLine : INetcoreBasic
    {
        public ServiceProviderBanksLine()
        {
            this.createdAt = DateTime.Now.Date;
            this.isDefaultBank = false;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Service Provider Bank Id")]
        public string serviceProviderBanksLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Service Provider Id")]
        public string serviceProviderId { get; set; }
        
        [Display(Name = "ServiceProvider")]
        public ServiceProvider serviceProvider { get; set; }

        [StringLength(256)]
        [Display(Name = "Bank Name")]
        public string bankName { get; set; }

        [Display(Name = "Account Number")]
        [StringLength(50)]
        public string accountNumber { get; set; }

        [Display(Name = "Account Holder Name")]
        [StringLength(256)]
        public string accountHolderName { get; set; }

        [Display(Name = "IFSC Code")]
        [StringLength(50)]
        public string ifscCode { get; set; }

        [Display(Name = "Current Bank ?")]
        public bool isDefaultBank { get; set; } = false;

    }
}
