﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum LocationType
    {   
        [Display(Name = "Factory / Warehouse")]
        Factory_Warehouse  = 1,

        [Display(Name = "Business")]
        Business = 2,

        [Display(Name = "Residential Area")]
        Residential = 3,

        [Display(Name = "Sea Port")]
        Port = 4,

        [Display(Name = "Air Port")]
        Airport = 5
    }

}
