﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Quotation")]
    public class Quotation : INetcoreMasterChild
    {
        public Quotation()
        {
            this.createdAt = DateTime.Now + DateTime.Now.TimeOfDay;
            this.quotationNumber = "QT" + Guid.NewGuid().ToString().Substring(0, 5).ToUpper();
            this.quotationDate = DateTime.Now + DateTime.Now.TimeOfDay;
            this.quotationExpiryDate = this.quotationDate.AddDays(10);
            this.quotationStatus = QuotationStatus.Draft;

            this.etd = DateTime.Now.Date;
            this.eta = DateTime.Now.Date.AddDays(10);
        }

        [StringLength(38)]
        [Display(Name = "Quotation Id")]
        [Key]
        public string quotationId { get; set; }

        [StringLength(125)]
        [Required]
        [Display(Name = "Quotation Number")]
        public string quotationNumber { get; set; }

        [Display(Name = "Quotation Date")]
        public DateTime quotationDate { get; set; }

        //[Display(Name = "Shipment Name")]
        //public string shipmentName { get; set; }

        [Required]
        [Display(Name = "Shipment Name")]
        public int shipmentTrackingId { get; set; }

        [Display(Name = "Shipment Name")]
        public ShipmentTrackingLinks shipmentTrackingLinks { get; set; }


        [Display(Name = "ETD")]
        public DateTime etd { get; set; }

        [Display(Name = "ETA")]
        public DateTime eta { get; set; }

        [Display(Name = "Transit Days")]
        public string transitDays { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Expedition Type")]
        public ExpeditionType expeditionType { get; set; }


        [Display(Name = "Quotation Expiry Date")]
        public DateTime? quotationExpiryDate { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Enquiry Number")]
        public string enquiryId { get; set; }

        [Display(Name = "Enquiry")]
        public Enquiry enquiry { get; set; }


        [Display(Name = "Consignee")]
        public string consignee { get; set; }

        [Display(Name = "Shipper")]
        public string shipper { get; set; }


        [StringLength(38)]
        [Required]
        [Display(Name = "Service Provider Id")]
        public string serviceProviderId { get; set; }

        [Display(Name = "Service Provider")]
        public ServiceProvider serviceProvider { get; set; }



        [Display(Name = "Terms")]
        public string terms { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Mode Of Transport")]
        public ModeOfTransport modeOfTransport { get; set; }

        [Display(Name = "Origin of Shipment Id")]
        public int? originOfShipmentId { get; set; }

        [NotMapped]
        public string originOfShipmentLocationName { get; set; }

        [NotMapped]
        [Display(Name = "Origin Pincode of Shipment")]

        public string originPickUpAddressPincode { get; set; }

        [NotMapped]
        public string destinationOfShipmentLocationName { get; set; }

        [NotMapped]
        [Display(Name = "Destination Pincode of Shipment")]
        public string destinationAddressPincode { get; set; }

        [Display(Name = "Origin of Shipment")]
        [ForeignKey("originOfShipmentId")]
        public Location originOfShipment { get; set; }

        [Display(Name = "Destination of Shipment Id")]
        public int? destinationOfShipmentId { get; set; }

        [Display(Name = "Destination of Shipment")]
        [ForeignKey("destinationOfShipmentId")]
        public Location destinationOfShipment { get; set; }

        [Display(Name = "Additional Information")]
        public string additionalInformation { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Quotation Status")]
        public QuotationStatus quotationStatus { get; set; }

        [Display(Name = "Quotation Charges Line")]
        public List<QuotationChargesLine> quotationChargesLine { get; set; } = new List<QuotationChargesLine>();

        [Display(Name = "Quotation Container Line")]
        public List<QuotationContainerLine> quotationContainerLine { get; set; } = new List<QuotationContainerLine>();
    }
}
