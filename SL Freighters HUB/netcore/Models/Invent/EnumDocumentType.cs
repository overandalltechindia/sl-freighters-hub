﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum DocumentType
    {   

        [Display(Name = "Booking Document")]
        Booking_Doc = 1,

        [Display(Name = "Shipping Document")]
        Shipping_Doc = 2,

        [Display(Name = "PAN Card")]
        PANCard = 3,

        [Display(Name = "Adhaar Card")]
        AdhaarCard = 4,

    }
}
