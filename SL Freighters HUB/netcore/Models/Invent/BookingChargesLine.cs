﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class BookingChargesLine : INetcoreBasic
    {
        public BookingChargesLine()
        {
            this.createdAt = DateTime.Now.Date;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Booking Charges Line Id")]
        public string bookingChargesLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [Display(Name = "Charge Name")]
        public string chargeName { get; set; }

        [Display(Name = "Amount")]  
        public decimal amount { get; set; }
    }
}
