﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models
{
    public class Notifications
    {
        [StringLength(38)]
        [Display(Name = "Notification Id")]
        [Key]
        public string NotificationId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Link")]
        public string Link { get; set; }

        [Display(Name = "From User Id")]
        public string fromUserId { get; set; }
        [Display(Name = "From User")]
        public ApplicationUser fromUser { get; set; }

        [Display(Name = "To User Id")]
        public string toUserId { get; set; }
        [Display(Name = "To User")]
        public ApplicationUser toUser { get; set; }

        [Display(Name = "Data")]
        public string Data { get; set; }

        public DateTime createdDate { get; set; }
    }
}
